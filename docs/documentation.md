![](docs/logo.jpeg)

# Documentation
The SYN-Project mainly involves two software components.  
First, the **SYN-Trainer** which manages tasks in experiments for the text domain using a program cache and offers the ability to translate DreamCoders' synthesized program to Nim-code.  
Second, the **SYN-DC-Adapter** which is a Python script, allocated in the DreamCoder installation, which is used as entry point for DreamCoder.  
The following diagram simplifies the functionality of SYN-Trainer and SYN-DC-Adapter on a system level.  

![Funktionaler Entwurf](docs/project_overview.png)


## SYN-Trainer
### CLI
```shell
./syntrainer help
Usage:
    syntrainer (-h | --help)
    syntrainer task list
    syntrainer task create <name> <pathToDataCSV>
    syntrainer task delete <name>
    syntrainer experiment list
    syntrainer experiment run <name>
    syntrainer experiment create <name> <settingFileName> <taskName>...
    syntrainer experiment delete <name>
    syntrainer getSolutions
    syntrainer translateSolution <taskName>
    syntrainer --version
```

* `syntrainer -h` Displays the help screen
* `syntrainer task list` Displays all tasks 
* `syntrainer task create <name> <pathToDataCSV>` Creates a task called `<name>` from a given SCV file 
* `syntrainer task delete <name>` Deletes the task `<name>`
* `syntrainer experiment list` Displays all experiments
* `syntrainer experiment run <name>` Runs the experiment `<name>` by starting the syndcadapter
* `syntrainer experiment create <name> <settingFileName> <taskNames>...` Creates an experiment called `<name>` with the given task names and loads the settings from `<settingFileName> `
* `syntrainer experiment delete <name>` Deletes the experiment `<name>`
* `syntrainer getSolutions` Imports the solutions of the dreamcoder (reading compressionMessages), writes them in the `solved` directory of the storage and updates the cache
* `syntrainer translateSolution <taskName>` translates the solution for the given `<taskName>` (it must be imported first) to Nim


### API (api.nim)
![](docs/api_synthesis_diagram.png)

**Description of api usage:**
1. Call `initialize()` to init storage etc.
2. Call `createAndRunTasksinNewExperiment(pathToDataCSVs: seq[string])` to create new tasks from the given csv files and run them.  
Return: Names of the created tasks
3. Call `update()` regularly. The retured integer tells how many tasks have been solved in the meantime.
4. If `update()` returns a value greater than `0`, access the first solved taks in the queue by calling `getSolvedTask()`. (By calling it again, you access the second solved task etc.)
5. Optional: Call `getOutputsForSolvedTaskIdent(ident: SynResultIdent, inputs: seq[ValueType])` to apply the found programm to all the other input examples. `SynResultIdent` is a member of `SYNTranslationResult` which identifies the solution in the cache.
6. Call `close()` to ensure that the DC process is closed.

### Project Structure

| directory          | meaning                                                              |
| ------------------ | -------------------------------------------------------------------- |
| /src               | source code files. Contains main-file, syntrainer.nim of SYN-Trainer |
| /src/syntrainerpkg | source files of SYN-DC-Adapter                                       |
| /tests             | tests for syntrainer                                                 |
| /tb1_cases         | example data to use the syntrainer on; script to create in-out data  |
| /dreamcoder_files  | several files used in or with the dreamcoder                         |
| /docs              | several documents belonging to the project, eg. graphs               |
| /demo              | contains the demo programm and the used data                         | 

#### Module overview
![](docs/module_overview.png)

For a detailed list and description of all modules, visit the [Detailed module description](#detailed-module-description) section.


### Installation and Configuration

#### Enviromental Variables
The enviromental variables can be configured in a `syntrainer.env` in the same directory as the binary.

| Constant                          | Example                             | Description                                                                                                                                                                                                                              |
| --------------------------------- | ----------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| STORAGEPATH                       | /path/to/a/new/directory            | All experiments and setting-files are stored here. Absolute paths and relative (to the binary) are possible  . Tasks are stored in the "tasks" subdir, settings are stored in the "setting_files" subdir                                 |
| STORAGETYPE                       | JSON                                | For now, we only offer a JSON-file based storage                                                                                                                                                                                         | 
| JSON_PRETTY_PRINTING              | FALSE                               | If TRUE, json objects are saved with indent for better readability. Put FALSE for better performance                                                                                                                                     |
| WRITE_DCADAPTER_OUTPUT_ON_CONSOLE | TRUE                                | If TRUE, output of syndcadapter is written on the console. Useful for debugging/testing                                                                                                                                                  |
| COMMAND_STRING                    | "python" or "container.img;python3" | Command string for starting syndcadapter.py in new process. Syntax:       "command;arg1;arg2". Args are optional and not limited in number. "syndcadapter.py", the experiment name and  STORAGEPATH is always appended to the arguments. |
| DCPATH                            | /path/to/ec                         | is used to access dreamcoder installation.                                                                                                                                                                                               |


#### Setup
1. `syntrainer.env` allows you to configure the enviromental variables
for your compiled syntrainer program: put the file in your installation directory.
3. call `nimble build`
4. call `syntrainer` according to the CLI usage


## Tests

Each test file describes a user story of how our application might be used.  

All tests have a source directory that containes all information needed to run the test.  

Each test suite first initializes its environment by creating a temporary `temp` dir where all tests are executed and which is deleted after the test.  
This ensures that the tests operate completely independant.  

For more details about the tests, visit the [detailed test descprition](#detailed-description-of-the-tests) below.


## 1-Click Demo
To guarantee the integrity and functionality of SYN-Trainer, one can use the following demo.

Run `nimble demo` (in the main direcotry) to start the demonstration.  
This uses a DC-Mock which simulates that the DC found a solution and then continues with the parsing and translation.

Or run `nimble demo <pathToDC>` which actually starts the DC.  
We give the DC 70 seconds to solve the tasks (but only tb1_initials needs that much time). Then the tasks are requested again and the cache returnes the solutoin immediatly.


## SYN-DC-Adapter
### Installation and Configuration


#### Setup
1. Copy all files from `dreamcoder_files/bin` into the subdirectory `bin` of the DC-installation
2. Copy `program.ml` from `dreamcoder_files/solvers` into the subdirectory `solvers` of the DC-installation
3. Rebuild ocaml solver of DC (eg. by calling `./container.img make`)

### Usage
You can either manually run the SYN-DC-Adapter or you let SYN-Trainer do the job.  
The manual way works as follows:  
1. Change directory to DC-installation, i.e. `cd ec`
2. Run `singularity exec container.img bin/syndcadapter.py <experimentName> <pathToStorage>` or
`./container.img python3 bin/syndcadapter.py <experimentName> <pathToStorage>`. SYN-DC-Adapter will search for the experiment in the configured storage.

Use the SYN-Trainer directly to manage the DreamCoder process:
1. Change directory to syntrainer installation (binary)
2. Run `./syntrainer run <experimentName>` which starts a process of SYN-DC-Adapter or in other words the whole DC-algorithm.

#### Options

The SYN-DC-Adapter has some constants which can be theoretically modified, but they depend on the SYN-Trainer's build and thus it is not recommended to change them, without changing code (i.e. constants) in SYN-Trainer's
source code files.

### Creating Examples

#### Create data with text-domain in CSV-Format and use them to create tasks with SYN-Trainer
* `create_testdata.py` is a script to generate input-output data for the dreamcoder to learn from. Simply call `main(input csv path, function = lambda x:..., output csv path)`
* in csv_data currently lie 3 files:
    - inventory.csv is a 8-d csv with various items/names/categories/numbers/etc
    - names.csv contains name, surname 
    - schools.csv is a 8-d csv file wit various data about some schools 
* in in_out_data are currently 4 files, each being a 2-d csv containing a line from one of the files in csv_data and some output generated from create_testdata.py

| output file          | input file             | lambda function [Python]                                                                        |
|:-------------------- |:---------------------- | ----------------------------------------------------------------------------------------------- |
| initials.csv         | names.csv              | lambda x: _append(_index(1)(_index(1)(_split(',')(x))))((_index(0)(_index(0)(_split(',')(x))))) |
| initials_example.csv | names.csv (lines 1-50) | (same as initials.csv)                                                                          |
| surname.csv          | names.csv              | lambda x: _index(0)(_split(',')(x))                                                             |
| item_category.csv    | inventory.csv          | lambda x: _append(_index(1)(_split(',')(x)))(_index(8)(_split(',')(x)))                         |
| toUpperSmall.csv     | names.csv              | lambda x: _upper(x)                                                                             | 


## Domains

### Modify text domain

1. Update `textdomain.py`
2. Update `ocaml`. (search for `(* syn *)`)  
Only necessary if you define a new primitive that is not implemented yet in the program.ml file.
3. Change directory to `ec`
4. Run `singularity container.img make clean`
5. `singularity container.img make`

For more details visit the follow-up documentation.


## Results with DreamCoder
(For more in depth information about difficulties, visit the [follow-up developer documentation](docs/follow-up_developer_documentation.md))


### Our initials goal (TB1_Cases) was:
Find the solution for the following tasks:
| taskName | example                      |
| -------- | ---------------------------- |
| toUpper  | Harry Potter -> HARRY POTTER |
| toLower  | Harry Potter -> harry potter |
| surname  | Harry,Potter -> Potter       |
| lastname | Harry,Potter -> Harry        |
| initials | Harry,Potter -> HP           | 



### tb1_toUpper
Found with DC  in one iteration.
Best guess:
```
HIT toUpperSmall w/ (lambda (upper $0)) ; log prior = -3.218876 ; log likelihood = 0.000000
```
Found programs in desc order of likelihood:
```lisp
(lambda (upper $0))
(lambda (upper (lower $0)))
(lambda (upper (upper $0)))
(lambda (upper (lower (upper $0))))
(lambda (upper (upper (upper $0))))
```

### tb1_surname

```lisp
tb1_surname
-0.31	(lambda (testsyn $0))
-2.25	(lambda (firstString (split ',' $0)))
-2.39	(lambda (testsyn (testsyn $0)))
-2.95	(lambda (nthString 0 (split ',' $0))) <-- this is what we want
-4.20	(lambda (firstString (split (caseUpper ',') $0)))
```




### tb1_initials
```lisp
tb1_initials
-1.18   (lambda (concatenate (chr2str (nthChar 0 (nthString 1 (split ',' $0)))) (chr2str (nthChar 0 $0))))
-1.40   (lambda (concatenate (chr2str (nthChar 0 (nthString 1 (split ',' $0)))) (sliceString 0 1 $0)))
-1.40   (lambda (sliceString 0 2 (concatenate (chr2str (nthChar 0 (nthString 1 (split ',' $0)))) $0)))
-1.62   (lambda (sliceString 0 2 (concatenate (sliceString 0 1 (nthString 1 (split ',' $0))) $0)))

```
This task was the hardest one (obviosly because of the number of necessary primitives).  
We found that it is found much faster (and that it even  makes the difference if the DC is able to find it at all) if you reduce the search space by removing some unnecessary primitives (eg. higher numbers, char constants, some functions).  




## Detailed module description

##### Translation
The translation module is based on similar python code in the DreamCoder repository (https://github.com/ellisk42/ec).   
`translateECProgramToNim()` internally only uses two procs:   
`parseECProgramStringToTree()` read the EC-Program-Expression expression and parses into a tree of ExpressionNodes.   
`translateProgramTreeToNim()` Then translates this tree into a tree of ProgramFragments which are of Abstraction, Application, Primitive or Parameter like this:  
surname: (lambda (nthString 0 (split ',' $0))) will resort in this tree:

<img src="docs/programFragmentTree_example.png" alt="drawing" width="400"/>

##### Controller
The controller coordinates access to modules like storage, translation or cache. It establishes high level access to these modules directly for CLI or API.

##### Interpreter
The interpreter module directly works with the Syntax Trees from the translation Module. It recursively walks down the tree and interprets the individual ProgramFragments. `call()` starts the chain downwards.

##### Cache
The cache module can search through all the saved programs previously synthesized by DC.   
`findSolvedTaskByIOExamples` will find a solved Task that fits the IOData.  
`findSolvedTaskByTaskName` will find a solved Task with a certain name.  
`update` will update the cached programs: it will save new programs that have been synthesized since last use

##### storage
storage defines the basic storage structure and functionality. At the moment it basically just provides json_storage functions but it's easily extended.

##### json_storage
Defines json specifig file operations like reading and writing.

##### syncdadapter_starter
Starts the synDCAdapter, which is a python script in the DC Installation. With this modul we establish the bridge from syntrainer to DC.  
`startSynDcAdapter` makes DC run an experiment   
`waitForExitOfAllProcesses` ensures that all processes are closed in the end.

##### Experiment
Defines the Experiment type: Its variables and structure and basic functionality like `$`, `==` and `checkSettings()`

##### Task
Defines the Task type: Its variables and functions like `$`, `==` or `newTask()`

##### types
Defines all the other types: like `IOData`, `ProgramFragments`, `ValueType` and more 

##### csv_reader
Helper to read csv files to `seq[IOData]` or `(seq[ValueType], seq[ValueType])`

##### doc
Defines the docstring for CLI Usage.

##### domain_utility
Helpers mainly for type conversion.

##### domains
Bundles domains.

##### environment_variables
Defines standard settings for environment variables.

##### exceptions
Defines all exceptions.

##### helpers
Helpers like `isInt`, `isFloat` and `makeAbsolute(path)` (which should be used consistently).


### Detailed description of the tests

#### Tests for the API (tapi.nim)

| name   | asserted result      | description                                |
| --------------------------------------------------------------- | ------------------------------------------------------------- | --------------------------------------------------------------- | 
| createAndRunTasksInNewExperiment                                | syntrainer. createAndRunTasksInNewExperiment([dataSCV]).isSome | Checks that tasks and experiments are correctly created         |     |
| update - not yet solved                                         | syntrainer.update()==0                                        | ensures that `update()` returns 0 if no tasks are solved        | 
| update - task was in between solved                             | syntrainer.update() == 1                                      | after the simulation of task solving, update has to return 1    | 
| cache - createAndRunTasksInNewExperiment with known/solved task | syntrainer.getSolvedTask().isSome                             | checks if the cache is used if the task has already been solved |
| queue of solved task should be empty"                           | syntrainer.lenSolvedTasks() == 0                              | after each task was taken from the queue, it should be empty    |

#### Tests for the syntrainer (tsyntrainer.nim)
Because there are too many assertions, we describe it verbally for better readability.

| name                               | asserted result                                                              | description                                                                     |
| ---------------------------------- | ---------------------------------------------------------------------------- | ------------------------------------------------------------------------------- |
| directory and file setup           | checks that all directories are correclty created                            | these are basic procedures that are called every time the syntrainer is started |
| createAndSaveTask                  | checks that creating tasks works and that the created tasks exist in storage |                                                                                 |
| deleteTask                         | checks that the deleted task is gone                                         |                                                                                 |
| createAndSaveExperiment            | checks that creating experiments works and that they exist in storage        |                                                                                 |
| deleteExperiment                   | checks that deleted experiments are gone                                     |                                                                                 |
| runExperiment                      | checks that the output file exists                                           | this indicates that the process has been started successfully                   |
| importECProgramsOfSolvedTasks      | checks if the imported solution exists in the storage                        | after the call of the mock function, the pseudo solutions can be imported       |
| getTranslatedECProgramOfSolvedTask | checks if the translation of the solution is correct                         |                                                                                 | 

#### Tests for the interpreter (tinterpreter.nim)

This test modules ensures that the parsing of the EC-Program-Expression works correctly and that they produce the correct output if they are excecuted.

#### Tests for the translation (ttranslation.nim)

This test modules checks if the Nim translation is correct be comparing the result with actual nim code.

#### Tests for the experiment class (texperiment.nim)

Here we have tests for the basic type functions like `==` for experiments and tasks.

#### Tests for the json_storage (tjson_storage.nim)

This tests low level json storage operations like loading and saving tasks or experiments.

