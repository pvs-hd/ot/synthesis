# Follow-Up Developer Documentation

## Domains

Due to a lack of time, it was not possible to intetegrate the Regex Strings domain, but we focused in the development process on the ability to cover and integrate more than one domain. For a continuation of this project with new members it would be a good start to add this domain.

### Modify text domain

1. Update `text.py`, `textdomain.py` which is defines the domain for DreamCoder's Python frontend
3. Update `ocaml`. (search for `(* syn *)`) Only necessary if you define a new primitive that is not implemented yet in the program.ml file.
3. Update the DreamCoder installation with `nimble setupDC <dcPath>`
4. Now we can add the modify the primitives for SYN-Trainer:
    a. Modify `domains/text.nim` (for the interpreter and translation modules)
    b. Modify `domains/text_compile.nim` to compile syntheiszed Nim programs. Those primitivers are exported by SYN-trainer and thus can be imported with the library.
    

### Where are the primitives defined?
* `textdomain.py` containes the primitives that the DC is allowed to use
* `program.ml` contains the definition of the primitives (again) in ocaml which is necessary for the DC
* `api_textdomain.nim` contains the primitives in nim which is needed to compile the translated nim programs
* `textdomain.nim ` uses the primitives to interpret the parsed programs

### Add a new domain
1. To add a new domain to DreamCoder it can be helpful to read [introduction](https://github.com/ellisk42/ec/blob/master/docs/creating-new-domains.md)
2. To integrate the new domain, the following will guide you through the tasks. We call the new domain `alpha`
    a. Add the alpha domain for the translation and interpreter module by creating the primitives in a file called `domains/alpha.nim` with the help of the templates define in `domain_utility.nim`.
    b. Add the domain to the exported primitives such that one can compile his synthesized programs: create `domains/alpha_compile.nim`
    c. Now update `domains.nim`
3. In every experiment we store the used domain. This is copied by the settings_default. The value is only evaluated by the SYN-DC-Adapter to import the domain-specific primitives wich then are used by the DreamCoder algorithm. For now, the only domain is `text`

## Results with DreamCoder (log book)
It was very time consuming to actually bring DreamCoder to work with the text domain. This little "diary" should help to integetrage new primitives or even a complete new domain in DreamCoder beside the [introduction](https://github.com/ellisk42/ec/blob/master/docs/creating-new-domains.md) for creating new domains in the DreamCoder Installation.

### Our initials goal (TB1_Cases) was:
Find the solution for the following tasks:
| taskName | example                      |
| -------- | ---------------------------- |
| toUpper  | Harry Potter -> HARRY POTTER |
| toLower  | Harry Potter -> harry potter |
| surname  | Harry,Potter -> Potter       |
| lastname | Harry,Potter -> Harry        |
| initials | Harry,Potter -> HP           | 



### tb1_toUpper
with DC and SYN-DC-Adapter manually via container in one iteration.
Best guess:
```lisp
HIT toUpperSmall w/ (lambda (upper $0)) ; log prior = -3.218876 ; log likelihood = 0.000000
```
Found programs in desc order of likelihood:
```lisp
(lambda (upper $0))
(lambda (upper (lower $0)))
(lambda (upper (upper $0)))
(lambda (upper (lower (upper $0))))
(lambda (upper (upper (upper $0))))
```

### tb1_surname
This task, compared to toUpperSmall was way harder. To come up with correct primitives
and a working configuration (of the experiment/DC), we introduced helper primitives
`firstString` and `testsyn` which are compositions of the primitives `0`, `split`, `nthString`, `,`.
Thus the search space for DC was shrinked and we war able to check if DC can handle this task
with regarding the input data and the ocaml code itself. Finally we came up this nice result:
```lisp
tb1_surname
-0.31	(lambda (testsyn $0))
-2.25	(lambda (firstString (split ',' $0)))
-2.39	(lambda (testsyn (testsyn $0)))
-2.95	(lambda (nthString 0 (split ',' $0))) <-- this is what we want
-4.20	(lambda (firstString (split (caseUpper ',') $0)))
```

But as we adjusted the parameters (mainly reducing `enumeration Timeout` wich corresponds to the number of enumerated programs) the Dc was able to find the correct programs much faster (as the solution is always the simplest programm that fits).



### tb1_initials
```lisp
tb1_initials
-1.18   (lambda (concatenate (chr2str (nthChar 0 (nthString 1 (split ',' $0)))) (chr2str (nthChar 0 $0))))
-1.40   (lambda (concatenate (chr2str (nthChar 0 (nthString 1 (split ',' $0)))) (sliceString 0 1 $0)))
-1.40   (lambda (sliceString 0 2 (concatenate (chr2str (nthChar 0 (nthString 1 (split ',' $0)))) $0)))
-1.62   (lambda (sliceString 0 2 (concatenate (sliceString 0 1 (nthString 1 (split ',' $0))) $0)))

```
This task was the hardest one (obviosly because of the number of necessary primitives).
We found that it is found much faster (and that it even  makes the difference if the DC is able to find it at all) if you reduce the search space by removing some unnecessary primitives (eg. higher numbers, char constants, some functions).

### Collected knowledge
* You don't neet much data, 10-20 examples are enough
* some things are mysterious:
```lisp
tb1_1st+3rd_char        ( -> Why is this working but not 1st_char or 3rd_char?)
-0.88   (lambda (concatenate (chr2str (nthChar 0 $0)) (chr2str (nthChar 2 $0))))
-1.23   (lambda (concatenate (sliceString 0 1 $0) (chr2str (nthChar 2 $0))))
-1.23   (lambda (concatenate (chr2str (nthChar 0 $0)) (sliceString 2 3 $0)))

```

```lisp
tb1_2charsAfterColon                  -> But tb1_1charAfterColon doesn't work
-0.10   (lambda (sliceString 0 2 (nthString 1 (split ',' $0))))
-3.69   (lambda (sliceString 0 2 (concatenate (nthString 1 (split ',' $0)) $0)))
-3.69   (lambda (sliceString 0 2 (nthString 1 (split ',' (concatenate $0 $0)))))
-3.69   (lambda (sliceString 0 2 (nthString 2 (split ',' (concatenate $0 $0)))))
-3.84   (lambda (firstString (split LPAREN (sliceString 0 2 (nthString 1 (split ',' $0))))))

```
So we concluded that (for whatever reason) the DC doesn't work if the result consists of only one character.

* Another observation: When we tried to force him to use the first char by giving one example that has only one char, an error occured:
```
Traceback (most recent call last):
  File "/home/container/ec_syn/bin/../dreamcoder/enumeration.py", line 302, in solveForTask_ocaml
    response = json.loads(response.decode("utf-8"))
  File "/usr/local/conda/lib/python3.8/json/__init__.py", line 357, in loads
    return _default_decoder.decode(s)
  File "/usr/local/conda/lib/python3.8/json/decoder.py", line 337, in decode
    obj, end = self.raw_decode(s, idx=_w(s, 0).end())
  File "/usr/local/conda/lib/python3.8/json/decoder.py", line 355, in raw_decode
    raise JSONDecodeError("Expecting value", s, err.value) from None
json.decoder.JSONDecodeError: Expecting value: line 1 column 1 (char 0)

```

## Software Architecture
In the following we make some points on the software architecture we have so far.
- The `api.nim` module was meant to be simple such that the OpenTable project can integrate us easily. With multiple domains it might be nesseccary to diffrentiate in the task specific domains. So far we asumed it is the text-domain and therefore we use by default the setting_default in the creation of experiments (see proc: `createAndRunTasksInNewExperiment`). We propose to make the task-specific domain part of the argument such that the task can be created and added to a domain-specific experiment.


