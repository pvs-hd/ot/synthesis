##    syntrainer.nimble
##    define nimble commands for syntrainer
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

# Package

version       = "0.3.0"
author        = "C. Schön-Schöffel, F. von Manstein, J. Borowitz"
description   = "SYN-Trainer for training and using DreamCoder"
license       = "None"
srcDir        = "src"
bin           = @["syntrainer"]
installExt    = @["nim"]


# Dependencies

requires "nim >= 1.4.6"
requires "docopt >= 0.6.5"
requires "dotenv >= 1.1.0"

import os, strutils, strformat

let dcConfigureScript = "dreamcoder_files/install.py"
let createTestDataScript = "create_testdata.py"
let envFile = "syntrainer.env"
let python3Path = "python3"

task setupSYN, "Initialization of SYN-Trainer":
    ## usage `nimble setupSYN <appDir>`
    let argPosENVPath = 9
    if paramCount() < argPosENVPath:
        quit("Path to application is missing.")
    exec "cp example." & $envFile & " " & $(paramStr(argPosENVPath)/envFile)
    echo "You can now modify and configure '" & $envFile & "'."
    echo "After it, if DreamCoder is not configured yet, run 'nimble setupDC <pathToDC>'"

task setupDC, "Configuration of DreamCoder":
    ## usage `nimble setupDC <pathToDC>`
    let argPosDCPath = 9
    if paramCount() < argPosDCPath:
        quit("DreamCoder installation path is missing.")

    echo "Running Configuration-Script for DC-installation at"
    exec python3Path & " " & $dcConfigureScript & " " & paramStr(argPosDCPath)

task createTestDataCSVs, "Create Input-Out-Example CSVs for TB1 cases (text domain)":
    ## usage `nimble createTestDataCSVs <outPath>`
    let argPosOutPath = 9
    if paramCount() < argPosOutPath:
        quit("Output path is missing.")
    echo "Running Create-Test-Data Script"
    exec python3Path & " " & createTestDataScript & " " & paramStr(argPosOutPath)

proc rmFiles(dir: string) =
    for file in listFiles(dir):
        rmFile(file)

task demo, "Run 1-Click-Demo":
    ## usage `nimble demo (<pathToDC>)`
    exec "nimble createTestDataCSVs demo/demoFiles/data"
    exec "nimble setupSYN demo"
    let argPosDCPath = 9
    if paramCount() < argPosDCPath:
        exec "nim c --threads:on -r demo/demo.nim"
    else:
        exec "nim c --threads:on -r demo/demo.nim " & paramStr(argPosDCPath)
    rmFiles("demo/demoFiles/data")
    rmFile("demo"/envFile)
