##    tinterpreter.nim
##    testing procedures in interpreter.nim
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import unittest, options

import syntrainerpkg/[interpreter, translation, types, domain_utility]

suite "Interpreter test suite":
    test "call - first sample - list domain":
        var ecProgram: ProgramFragment = parseECProgramStringToTree("(lambda (first (map (lambda $0) $0)))")
        var input = ValueType(kind: ecSeq, seqVal: @[ValueType(kind: ecString, stringVal: "Test"), ValueType(kind: ecString, stringVal: "Test2")])

        var res = call(Abstraction(ecProgram), input)
        check:
            res.isSome and res.get() == ValueType(kind: ecSeq, seqVal: @[ValueType(kind: ecString, stringVal: "Test")])

    test "call - second sample - text domain":
        var ecProgram = parseECProgramStringToTree("(lambda (concatenate (chr2str (nthChar 0 $0)) (chr2str (nthChar 2 $0))))")
        var input = ValueType(kind: ecString, stringVal: "Mustermann,Max")

        var res = call(Abstraction(ecProgram), input)
        check:
            res.isSome and res.get() == ValueType(kind: ecString, stringVal: "Ms")

    test "call - third sample - text domain":
        var ecProgram = parseECProgramStringToTree("(lambda (nthString 0 (split ',' $0)))")
        var input = ValueType(kind: ecString, stringVal: "Mustermann,Max")

        var res = call(Abstraction(ecProgram), input)
        check:
            res.isSome and res.get() == ValueType(kind: ecString, stringVal: "Mustermann")

    test "call - fourth sample - text domain":
        var ecProgram = parseECProgramStringToTree("(lambda (concatenate (chr2str (nthChar 0 (nthString 1 (split ',' $0)))) (chr2str (nthChar 0 $0))))")
        var input = ValueType(kind: ecString, stringVal: "Mustermann,Max")

        var res = call(Abstraction(ecProgram), input)
        check:
            res.isSome and res.get() == ValueType(kind: ecString, stringVal: "MM")

    test "call - fifth sample - text domain":
        var ecProgram = parseEcProgramStringToTree("(lambda (sliceString 0 (incr (incr (incr (incr 1)))) $0))")
        var input = ValueType(kind: ecString, stringVal: "25560 Stadt")

        var res = call(Abstraction(ecProgram), input)
        check:
            res.isSome and res.get() == ValueType(kind: ecString, stringVal: "25560")

    # falsche datentypen testen
