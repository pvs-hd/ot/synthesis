##    helpers_for_tests.nim
##    establish helper functions for unittesting
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import os, strUtils

proc mockSYNDCAdapterSolvedTask*(tempDirEC: string, sourceDir: string, tasksToSolve: seq[string], solvedTaskNames: seq[string]) =
    # TODO copy compressionMessage.json to temp_testsuite_ec and replace taskName in file with current
    var messageName: string = "test_compressionMessage"
    var comMessagePath: string = tempDirEC/"compressionMessages"/messageName&".json"

    os.copyFile(sourceDir/"compressionMessage.json",comMessagePath)

    var buffer = ""
    for line in comMessagePath.lines:
        var line = line
        for i in countup(0, solvedTaskNames.len-1):
            line = line.replace("\x22" & tasksToSolve[i] & "\x22", "\x22" & solvedTaskNames[i] & "\x22")
        line &= '\x0A'
        buffer.add(line)
    writeFile(comMessagePath, buffer)
