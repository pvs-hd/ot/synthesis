
# imports
import sys, json, os, datetime, re
import time
  


#for testing:
SYN_T_STORAGEPATH = os.getcwd()+"/../storage/"

# SYN-DC-Adapter
# identifier in experiment json object
EXPERIMENT_SETTINGS_IDENT = "settings" # where are specific dreamcoder settings
EXPERIMENT_OUTDIR_IDENT = "outdir" # where to store DreamCoder results
EXPERIMENT_DOMAIN_IDENT = "domain" # default "text" domain for TB1Cases
EXPERIMENT_TASKS_IDENT = "tasks" # is array with task names

TASK_EXAMPLE_INPUT_IDENT = "i"
TASK_EXAMPLE_OUTPUT_IDENT = "o"
TASK_EXAMPLE_IDENT = "data"

# hardware specific settings
NUMBER_OF_CPUS = 1

# usage: python syndcadapter.py <experimentFileName>
# example: python syndcadapter.py experiment1
# syn-dc-adapter will execute the experiment
def main():

    # get experiment
    experimentFileName = sys.argv[1]
    pathToExperiment = os.path.join(SYN_T_STORAGEPATH, experimentFileName+".json")
    experiment = getJsonObject(pathToExperiment)

    settings = experiment[EXPERIMENT_SETTINGS_IDENT]
    settings["CPUs"] = NUMBER_OF_CPUS
    args = settings

    outDir = experiment[EXPERIMENT_OUTDIR_IDENT]
    initOutDir(outDir)
    outPrefix = getOutPrefix(outDir)
    args.update({"outputPrefix": outPrefix})

    training_data = getTasks(experiment[EXPERIMENT_TASKS_IDENT])
    print("Experiment successfully loaded:")
    print(training_data)


def getTasks(taskNamesList):
    tasks = []
    for taskName in taskNamesList:
        pathToJsonFile = os.path.join(SYN_T_STORAGEPATH, "tasks", taskName+".json")
        tasks.append(getJsonObject(pathToJsonFile))
    return tasks

def getJsonObject(pathToJsonFile):
    jsonObject = None
    try:
        jsonFile = open(pathToJsonFile, "r")
        try:
            jsonObject = json.load(jsonFile)
        except:
            print("Loading .json file failed (invalid Json): " + pathToJsonFile)
        finally:
            jsonFile.close()
            return jsonObject
    except (IOError, ValueError, EOFError) as e:
        print(e)

def getOutPrefix(outDir):
    return outDir


def initOutDir(outDir):
    os.makedirs(outDir, exist_ok=True)

if __name__ == "__main__":
    print("Syndcadapter started")
    main()

