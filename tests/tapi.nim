##    tapi.nim
##    testing procedures in api.nim
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import unittest, os, options
import syntrainerpkg/[helpers, csv_reader, api]
import ../tests/helpers_for_tests

var createdTaskNames: seq[string] = @[]


let testFilesDir = makeAbsolute("testFiles")
let sourceDir = makeAbsolute("tapiFiles")

let tempDir = makeAbsolute("temp")
let tempDirEC = tempDir/"temp_testsuite_ec"
let tempStoragePath = tempDir/"storage"
let tempDirOut = tempDir/"temp_exp_outdir" #This path must match the outdir in tsyntrainer/setting_default.json
let IODataFile = sourceDir/"initials50tapi.csv"


suite "API test suite":

    if os.dirExists(sourceDir):
        os.removeDir(sourceDir)

    os.createDir(tempDir)
    os.copyDir(testFilesDir, sourceDir)

    if os.dirExists(tempDir):
        os.removeDir(tempDir)

    os.createDir(tempDir)
    os.createDir(tempStoragePath)


    os.createDir(tempDirOut)

    os.createDir(tempDirEC)
    os.createDir(tempDirEC/"compressionMessages")
    os.createDir(tempDirEC/"bin")
    os.copyFile(sourceDir/"syndcadapter_forTesting.py", tempDirEC/"bin"/"syndcadapter.py")


    api.initialize(false)

    #Override the defualt setting file to that the ourdir is correct
    os.copyFileToDir(sourceDir/"setting_default.json", tempStoragePath/"setting_files")

    echo "API test suite initialized."

    test "test createAndRunTasksInNewExperiment":

        let inputOutputExampleFilePath = IODataFile

        echo inputOutputExampleFilePath & " exists=" & $os.fileExists(inputOutputExampleFilePath)
        var createdTaskNamesRes = api.createAndRunTasksInNewExperiment(@[inputOutputExampleFilePath])

        check:
            createdTaskNamesRes.isSome and len(createdTaskNamesRes.get()) == 1

        createdTaskNames = createdTaskNamesRes.get()

    test "test update - not yet solved":

        var numberSolvedTasks = api.update()

        check:
            numberSolvedTasks == 0

    mockSYNDCAdapterSolvedTask(tempDirEC, sourceDir, @["tb1_initials"], createdTaskNames)

    test "test update - task was in between solved":

        var numberSolvedTasks = api.update()
        var solvedTaskRes = api.getSolvedTask()

        var correctProgram: FormattedCodeLines = @[
        "proc lambda1(param1: auto): auto =",
        "   var param1 = param1",
        "   return concatenate(chr2str(nthChar(0)(nthString(1)(split(\',\')(param1)))))(chr2str(nthChar(0)(param1)))"
        ]


        check:
            numberSolvedTasks == 1
            solvedTaskRes.isSome and solvedTaskRes.get().translationResult == correctProgram

    var solved: SYNResultIdent
    test "test cache - createAndRunTasksInNewExperiment with known/solved task (before next update solved)":

        let inputOutputExampleFilePath = IODataFile

        var createdTaskNamesRes = api.createAndRunTasksInNewExperiment(@[inputOutputExampleFilePath])

        var solvedTransRes = api.getSolvedTask()

        var correctProgram: FormattedCodeLines = @[
        "proc lambda1(param1: auto): auto =",
        "   var param1 = param1",
        "   return concatenate(chr2str(nthChar(0)(nthString(1)(split(\',\')(param1)))))(chr2str(nthChar(0)(param1)))"
        ]
        check:
            solvedTransRes.isSome and solvedTransRes.get().translationResult == correctProgram

        solved = solvedTransRes.get().ident

    test "test getOutputsForSolvedTaskIdent":
        let inputOutputExampleFilePath = sourceDir/"initials50tapi.csv"

        let (input, output) = readValueTypes(inputOutputExampleFilePath)

        let generatedOutput = getOutputsForSolvedTaskIdent(solved, input)

        check:
            generatedOutput == some(output)

    test "test lenSolvedTasks - queue of solved task should be empty":

        check:
            api.lenSolvedTasks() == 0

    api.close()
    os.removeDir(sourceDir)
    echo "Cleaned up test suite directories"
