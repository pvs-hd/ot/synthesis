##    ttranslation.nim
##    testing procedures in translation.nim
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import unittest

import syntrainerpkg/translation

suite "translation test suite":
    test "first sample  - parseECProgramStringToTree":
        let testECProgram: string = "(lambda (concatenate (sliceString 0 1 $0) (sliceString 0 1 (nthString 1 (split ',' $0)))))"
        let programTree = parseECProgramStringToTree(testECProgram)

        check:
            $programTree == "Abstraction(identifier: 1, body: @[Application(program: Application(program: Primitive(name: concatenate), input: Application(program: Application(program: Application(program: Primitive(name: sliceString), input: Primitive(name: 0)), input: Primitive(name: 1)), input: Parameter(index: 0))), input: Application(program: Application(program: Application(program: Primitive(name: sliceString), input: Primitive(name: 0)), input: Primitive(name: 1)), input: Application(program: Application(program: Primitive(name: nthString), input: Primitive(name: 1)), input: Application(program: Application(program: Primitive(name: split), input: Primitive(name: ',')), input: Parameter(index: 0)))))])"

    test "second sample - parseECProgramStringToTree":
        let testECProgram: string = "(lambda (sliceString 0 4 $0))"
        let programTree = parseECProgramStringToTree(testECProgram)

        check:
            $programTree == "Abstraction(identifier: 1, body: @[Application(program: Application(program: Application(program: Primitive(name: sliceString), input: Primitive(name: 0)), input: Primitive(name: 4)), input: Parameter(index: 0))])"

    test "third sample  - parseECProgramStringToTree":
        let testECProgram: string = "(lambda (nthString 0 (split ',' $0)))"
        let programTree = parseECProgramStringToTree(testECProgram)

        check:
            $programTree == "Abstraction(identifier: 1, body: @[Application(program: Application(program: Primitive(name: nthString), input: Primitive(name: 0)), input: Application(program: Application(program: Primitive(name: split), input: Primitive(name: ',')), input: Parameter(index: 0)))])"

    test "first sample  - translateProgramTreeToNim":
        let testECProgram: string = "(lambda (concatenate (sliceString 0 1 $0) (sliceString 0 1 (nthString 1 (split ',' $0)))))"
        let programTree = parseECProgramStringToTree(testECProgram)
        let translation = translateProgramTreeToNim(programTree)

        check:
            translation == @[
                "proc lambda1(param1: auto): auto =",
                "   var param1 = param1",
                "   return concatenate(sliceString(0)(1)(param1))(sliceString(0)(1)(nthString(1)(split(\',\')(param1))))"]

    test "second sample - translateProgramTreeToNim":
        let testECProgram: string = "(lambda (sliceString 0 4 $0))"
        let programTree = parseECProgramStringToTree(testECProgram)
        let translation = translateProgramTreeToNim(programTree)

        check:
            translation == @[
                "proc lambda1(param1: auto): auto =",
                "   var param1 = param1",
                "   return sliceString(0)(4)(param1)"]

    test "third sample  - translateProgramTreeToNim":
        let testECProgram: string = "(lambda (nthString 0 (split ',' $0)))"
        let programTree = parseECProgramStringToTree(testECProgram)
        let translation = translateProgramTreeToNim(programTree)

        check:
            translation == @[
                "proc lambda1(param1: auto): auto =",
                "   var param1 = param1",
                "   return nthString(0)(split(\',\')(param1))"]


    test "first sample  - translateECProgramStringToNim":
        let testECProgram: string = "(lambda (concatenate (sliceString 0 1 $0) (sliceString 0 1 (nthString 1 (split ',' $0)))))"
        let translation = translateECProgramStringToNim(testECProgram)

        check:
            translation == @[
                "proc lambda1(param1: auto): auto =",
                "   var param1 = param1",
                "   return concatenate(sliceString(0)(1)(param1))(sliceString(0)(1)(nthString(1)(split(\',\')(param1))))"]

    test "second sample - translateECProgramStringToNim":
        let testECProgram: string = "(lambda (sliceString 0 4 $0))"
        let translation = translateECProgramStringToNim(testECProgram)

        check:
            translation == @[
                "proc lambda1(param1: auto): auto =",
                "   var param1 = param1",
                "   return sliceString(0)(4)(param1)"]


    test "third sample  - translateECProgramStringToNim":
        let testECProgram: string = "(lambda (nthString 0 (split ',' $0)))"
        let translation = translateECProgramStringToNim(testECProgram)

        check:
            translation == @[
                "proc lambda1(param1: auto): auto =",
                "   var param1 = param1",
                "   return nthString(0)(split(\',\')(param1))"]
