##    tjson_storage.nim
##    testing procedures in json_storage.nim
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz
##    Note: This test is not as clean as the other test suites.

import json, os, tables, unittest
import syntrainerpkg/[task, experiment, json_storage, types, helpers]

const fileEnding = ".json"

proc createJsonTask(name: string): string;
proc createExperimentJson(experimentName: string): string;
proc getTaskTestExamples(version: string = "initials"): seq[seq[string]];
proc getTaskTestData(version: string = "initials"): seq[IOData];
proc createTestExperiment(experimentName: string): Experiment;

let tempDir = makeAbsolute("temp")

suite "json_storage test suite":

    if os.dirExists(tempDir):
        os.removeDir(tempDir)

    os.createDir(tempDir)

    let tempTasksPath = tempDir/"storage"/"tasks"
    let tempSettingsPath = tempDir/"Storage"/"setting_files"

    if not os.dirExists(tempTasksPath):
        os.createDir(tempTasksPath)
    if not os.dirExists(tempSettingsPath):
        os.createDir(tempSettingsPath)

    test "test loadTask()":
        const jsonName = "TestLoadTask"
        var testTask = Task(name: jsonName, data: getTaskTestData())

        if not os.fileExists(tempDir / jsonName & fileEnding):
            saveTask(testTask, tempDir, true)

        check:
            loadTask(tempDir / jsonName & fileEnding) == testTask

    test "test saveTask()":
        var taskName1 = "task1"
        var taskName2 = "task2"
        let testTask1 = Task(name: taskName1, data: getTaskTestData("initials"))
        let testTask2 = Task(name: taskName2, data: getTaskTestData("surnames"))

        if not os.dirExists(tempDir):
            os.createDir(tempDir)

        if os.fileExists(tempDir / taskName1 & fileEnding):
            os.removefile(tempDir / taskName2 & fileEnding)

        json_storage.saveTask(testTask1, tempTasksPath, true)
        json_storage.saveTask(testTask2, tempTasksPath, true)

        check:
            readFile(tempTasksPath / taskName1 & fileEnding) == createJsonTask(taskName1)


    test "test loadExperiment()":
        const jsonName = "TestLoadExperiment"

        var testExperiment = createTestExperiment(jsonName)



        if not os.fileExists(tempDir / jsonName & fileEnding):
            saveExperiment(testExperiment, tempDir, true)



        check:
            loadExperiment(tempDir / jsonName & fileEnding, tempTasksPath) == testExperiment


    test "test saveExperiment()":
        const jsonName = "TestSaveExperiment"
        let testExperiment = createTestExperiment(jsonName)

        if not os.dirExists(tempDir):
            os.createDir(tempDir)

        if os.fileExists(tempDir / jsonName & fileEnding):
            os.removefile(tempDir / jsonName & fileEnding)


        saveExperiment(testExperiment, tempDir, prettyPrinting = true)

        check:
            readFile(tempDir / jsonName & fileEnding) == createExperimentJson(jsonName)

    test "test deleteTask()":
        const jsonName = "TestDeleteTask"
        var testTask = Task(name: jsonName, data: getTaskTestData())

        if not os.dirExists(tempDir):
            os.createDir(tempDir)
        saveTask(testTask, tempDir)

        if not os.fileExists(tempDir / jsonName & fileEnding):
            echo "Couldn't find file: " & tempDir / jsonName  & fileEnding
        deleteTask(tempDir/jsonName  & fileEnding)

        check:
            not os.fileExists(tempDir / jsonName & fileEnding)

    test "test deleteExperiment()":
        const jsonName = "TestDeleteExperiment"
        var testExperiment = createTestExperiment(jsonName)

        if not os.dirExists(tempDir):
            os.createDir(tempDir)
        saveExperiment(testExperiment, tempDir)

        if not os.fileExists(tempDir/jsonName & fileEnding):
            echo "Couldn't find file: " & tempDir / jsonName & fileEnding
        deleteExperiment(tempDir / jsonName  & fileEnding)

        check:
            not os.fileExists(tempDir / jsonName & fileEnding)

    os.removeDir(tempDir)


proc getTaskTestExamples(version: string = "initials"): seq[seq[string]] =
    case version
        of "initials":
            return @[@["Harry Potter", "HP"], @["Hermine Granger", "HG"], @["Ron Weasley", "RW"]]
        of "surnames":
            return @[@["Harry Potter", "Harry"], @["Hermine Granger", "Hermine"], @["Ron Weasley", "Ron"]]

proc getTaskTestData(version: string = "initials"): seq[IOData]=
    var testData: seq[IOData] = @[]
    var examples = getTaskTestExamples(version)
    for i in 0..<examples.len:
        testData.add((IOValue(kind: ioString, strVal: examples[i][0]), IOValue(kind: ioString, strVal: examples[i][1])))
    result = testData

proc createJsonTask(name: string) : string =
    #identation important!
    var examples = getTaskTestExamples()
    var task = %*
        {
            "name": name,
            "data": [
                {
                    "i": examples[0][0],
                    "o": examples[0][1]
                },
                {
                    "i": examples[1][0],
                    "o": examples[1][1]
                },
                {
                    "i": examples[2][0],
                    "o": examples[2][1]
                }
            ]
        }
    return pretty(task, indent = 4)

proc createExperimentJson(experimentName: string): string =
    var experiment = %*
        {
            "name": experimentName,
            "domain": "text",
            "outdir": "path",
            "settings": {
                "enumerationTimeout": 10,
                "activation": "tanh",
                "iterations": 10,
                "recognitionTimeout": 3600,
                "maximumFrontier": 10,
                "topK": 2,
                "pseudoCounts": 30.0,
                "helmholtzRatio": 0.5,
                "structurePenalty": 1,
                "testingTimeout": 0,
                "arity": 3
            },
            "tasks": [
                "task1",
                "task2"
            ]
        }
    return pretty(experiment, indent = 4)

proc createTestExperiment(experimentName: string): Experiment =
    let testSettings = {
                "enumerationTimeout": "10",
                "activation": "tanh",
                "iterations": "10",
                "recognitionTimeout": "3600",
                "maximumFrontier": "10",
                "topK": "2",
                "pseudoCounts": "30.0",
                "helmholtzRatio": "0.5",
                "structurePenalty": "1",
                "testingTimeout": "0",
                "arity": "3"
            }.toOrderedTable
    let testTask1 = Task(name: "task1", data: getTaskTestData("initials"))
    let testTask2 = Task(name: "task2", data: getTaskTestData("surnames"))
    result = Experiment(name: experimentName, domain: "text", tasks: @[testTask1, testTask2],
                    outdir: "path", settings: testSettings)
