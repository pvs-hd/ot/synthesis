##    texperiment.nim
##    testing procedures for experiment.nim
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import unittest, tables
import syntrainerpkg/[experiment, types, task, exceptions]

proc getTaskTestExamples(version: string = "initials"): seq[seq[string]];
proc getTaskTestData(version: string = "initials"): seq[IOData];
proc createTestExperiment(experimentName: string, iterations: string = "10"): Experiment;

suite "Test basic type operations":

    test "test task ==":
        var task0: Task
        let task1: Task = nil
        let task2 = Task(name: "task", data: @[(IOValue(kind: ioString, strVal: "input"), 
                                                IOvalue(kind: ioString, strVal: "output"))])
        let task3 = Task(name: "task", data: @[(IOValue(kind: ioString, strVal: "input"), 
                                                IOvalue(kind: ioString, strVal: "output"))])
        let task4 = task2
        #note: different data
        let task5 = Task(name: "task", data: @[(IOValue(kind: ioString, strVal: "input2"), 
                                                IOvalue(kind: ioString, strVal: "output2"))])

        check:
            task0 == task1
            task2 == task3
            task2 == task4
            not(task2 == task5)
            not(task2 == task0)

    test "test experiment ==":
        var exp0: Experiment
        let exp1: Experiment = nil
        let exp2 = createTestExperiment("testExperiment")
        let exp3 = createTestExperiment("testExperiment")
        let exp4 = exp2
        let exp5 = createTestExperiment("testExperiment2")
        let exp6 = createTestExperiment("testExperiment", "11")

        check:
            exp0 == exp1
            exp2 == exp3
            exp2 == exp4
            not(exp0 == exp2)
            not(exp2 == exp5)
            not(exp2 == exp6)
    
    test "experiment checkSettings":
        let exp1 = createTestExperiment("testExperiment")


        exp1.domain = "math" # any other then "text"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.domain = "text"
        check: 
            exp1.hasValidSettings()

        exp1.settings["enumerationTimeout"] = "10,5"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.settings["enumerationTimeout"] = "10"
        check: 
            exp1.hasValidSettings()

        exp1.settings["iterations"] = "5.5"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.settings["iterations"] = "5"
        check: 
            exp1.hasValidSettings()

        exp1.settings["recognitionTimeout"] = "3600,5"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.settings["recognitionTimeout"] = "3600"
        check: 
            exp1.hasValidSettings()

        exp1.settings["maximumFrontier"] = "10,5"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.settings["maximumFrontier"] = "10"
        check: 
            exp1.hasValidSettings()

        exp1.settings["topK"] = "2,5"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.settings["topK"] = "2"
        check: 
            exp1.hasValidSettings()

        exp1.settings["structurePenalty"] = "10,5"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.settings["structurePenalty"] = "10"
        check: 
            exp1.hasValidSettings()

        exp1.settings["testingTimeout"] = "20,5"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.settings["testingTimeout"] = "20"
        check: 
            exp1.hasValidSettings()

        exp1.settings["arity"] = "3,5"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.settings["arity"] = "3"
        check: 
            exp1.hasValidSettings()

        exp1.settings["pseudoCounts"] = "30.O"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.settings["pseudoCounts"] = "30.0"
        check: 
            exp1.hasValidSettings()

        exp1.settings["helmholtzRatio"] = "O.5"
        expect InvalidSettingsError:
            exp1.checkSettings()
        exp1.settings["helmholtzRatio"] = "0.5"

        check: 
            exp1.hasValidSettings()

proc getTaskTestExamples(version: string = "initials"): seq[seq[string]] =
    case version
        of "initials":
            return @[@["Harry Potter", "HP"], @["Hermine Granger", "HG"], @["Ron Weasley", "RW"]]
        of "surnames":
            return @[@["Harry Potter", "Harry"], @["Hermine Granger", "Hermine"], @["Ron Weasley", "Ron"]]

proc getTaskTestData(version: string = "initials"): seq[IOData]=
    var testData: seq[IOData] = @[]
    var examples = getTaskTestExamples(version)
    for i in 0..<examples.len:
        testData.add((IOValue(kind: ioString, strVal: examples[i][0]), IOValue(kind: ioString, strVal: examples[i][1])))
    result = testData

proc createTestExperiment(experimentName: string, iterations: string = "10"): Experiment =
    let testSettings = {
                "enumerationTimeout": "10",
                "activation": "tanh",
                "iterations": iterations,
                "recognitionTimeout": "3600",
                "arity": "3",
                "maximumFrontier": "10",
                "topK": "2",
                "pseudoCounts": "30.0",
                "helmholtzRatio": "0.5",
                "structurePenalty": "1",
                "testingTimeout": "400"
            }.toOrderedTable
    let testTask1 = Task(name: "task1", data: getTaskTestData("initials"))
    let testTask2 = Task(name: "task2", data: getTaskTestData("surnames"))
    result = Experiment(name: experimentName, domain: "text", tasks: @[testTask1, testTask2],
                        outdir: "path", settings: testSettings)