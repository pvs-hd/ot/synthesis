##    tsyntrainer.nim
##    testing procedures in syntrainer.nim
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import unittest, tables, algorithm, os, options
import syntrainerpkg/[task, controller, types, helpers]
import syntrainer
import ../tests/helpers_for_tests

let testFilesDir = makeAbsolute("testFiles")
let sourceDir = makeAbsolute("tsyntrainerFiles")
let tempDir = makeAbsolute("temp")
let tempDirEC = tempDir/"temp_testsuite_ec"
let tempStoragePath = tempDir/"storage"
let tempDirOut = tempDir/"temp_exp_outdir" #This path must match the outdir in tapi/setting_default.json
let IODataFile = sourceDir/"initials5tsyntrainer.csv"


suite "Syntrainer test suite":

    if os.dirExists(sourceDir):
        os.removeDir(sourceDir)

    os.createDir(tempDir)
    os.copyDir(testFilesDir, sourceDir)

    if os.dirExists(tempDir):
        os.removeDir(tempDir)

    os.createDir(tempDir)

    if os.dirExists(tempDir):
        os.removeDir(tempDir)

    os.createDir(tempDir)
    os.createDir(tempStoragePath)



    os.createDir(tempDirOut)
    os.createDir(tempDirEC)
    os.createDir(tempDirEC/"compressionMessages")
    os.createDir(tempDirEC/"bin")

    os.copyFile(sourceDir/"syndcadapter_forTesting.py", tempDirEC/"bin"/"syndcadapter.py")



    syntrainer.initialize(false)



    echo "Syntrainer test suite initialized."

    test "test directory and file setup":
        check:
            os.dirExists(tempStoragePath/"tasks")
            os.dirExists(tempStoragePath/"setting_files")
            os.fileExists(tempStoragePath/"setting_files"/"setting_default.json")
            os.dirExists(tempStoragePath/"solved")
            controller.listTasks().len() == 0
            controller.listExperiments().len() == 0

    #Override the defualt setting file to that the ourdir is correct
    os.copyFileToDir(sourceDir/"setting_default.json", tempStoragePath/"setting_files")


    test "test createAndSaveTask":

        var task1Res = controller.createAndSaveTask("test_task1", IODataFile)
        var task2Res = controller.createAndSaveTask("test_task2", IODataFile)
        var task3Res = controller.createAndSaveTask("test_task3", IODataFile)

        check:
            task1Res.isSome()
            task2Res.isSome()
            task3Res.isSome()

        var task1 = task1Res.get()
        var task2 = task2Res.get()
        var task3 = task3Res.get()

        var tasks = controller.listTasks()
        sort(tasks, system.cmp)
        check:
            task1.name == "test_task1" and task2.name == "test_task2" and task3.name == "test_task3"
            task1.data.len() == 5 #Requires that initials.csv is not modified
            tasks == @["test_task1", "test_task2", "test_task3"]
            controller.listExperiments().len() == 0

    test "test deleteTask":
        controller.deleteTask("test_task3")
        var tasks = controller.listTasks()
        sort(tasks, system.cmp)
        check:
            tasks == @["test_task1", "test_task2"]

    test "test createAndSaveExperiment":
        var exp1Res = controller.createAndSaveExperiment("test_experiment1", "setting_default", @["test_task1", "test_task2"])
        var exp2Res = controller.createAndSaveExperiment("test_experiment2", "setting_default", @["test_task1"])

        check:
            exp1Res.isSome
            exp2Res.isSome

        var exp1 = exp1Res.get()
        var exp2 = exp2Res.get()

        var experiments = controller.listExperiments()
        sort(experiments, system.cmp)

        check:
            exp1.name == "test_experiment1" and exp2.name == "test_experiment2"
            exp1.settings["recognitionTimeout"] == "3600" #Requires that setting_default.json is not modified
            exp1.domain == "text"
            experiments == @["test_experiment1", "test_experiment2"]

    test "test deleteExperiment":
        controller.deleteExperiment("test_experiment1")
        check:
            controller.listExperiments() == @["test_experiment2"]

    test "test runExperiment":
        controller.runExperiment("test_experiment2")
        check:
            true # Works if no error occurs and if the output is written on the console. (Requires: WRITE_DCADAPTER_ON_CONSOLE=TRUE)

    var taskName = "tb1_test_task"
    mockSYNDCAdapterSolvedTask(tempDirEC, sourceDir, @["tb1_initials"], @[taskName])

    test "test importECProgramsOfSolvedTasks":
        controller.importECProgramsOfSolvedTasks()
        check:
            os.fileExists(tempStoragePath/"solved"/taskName&".json") #file name given by the mock proc


    test "test getTranslatedECProgramOfSolvedTask":
        var nimProgramm = controller.getTranslatedECProgramOfSolvedTask(taskName)
        var correctProgram: FormattedCodeLines = @[
        "proc lambda1(param1: auto): auto =",
        "   var param1 = param1",
        "   return concatenate(chr2str(nthChar(0)(nthString(1)(split(\',\')(param1)))))(chr2str(nthChar(0)(param1)))"
        ]
        check:
            nimProgramm.isSome and nimProgramm.get() == correctProgram

    os.removeDir(sourceDir)
    echo "Cleaned up test suite directories"
