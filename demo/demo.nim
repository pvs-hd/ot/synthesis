##    demo.nim
##    One-Click-Demo for the synthesis project
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import unittest, os, sequtils, sugar, options, strutils
import syntrainerpkg/[environment_variables, helpers, json_storage, csv_reader]
import ../tests/helpers_for_tests
import syntrainer

let sourceDir = makeAbsolute("demoFiles")
let copyTestFiles = makeAbsolute("../tests/testFiles")
let localSyndcadapter = sourceDir/"syndcadapter_forTesting.py"
let tb1cases = @["tb1_surname", "tb1_lastname", "tb1_upper", "tb1_lower", "tb1_concat", "tb1_initials"]
let pathToCSVs = sourceDir/"data"

const waitSec = 70

var taskNames: seq[string] = @[]
var synResults: seq[SYNTranslationResult] = @[]


let tempDir = makeAbsolute("temp")

var dirEC = tempDir/"temp_testsuite_ec"
var useMockEC = true
let tempStoragePath = tempDir/"storage"
let tempDirOut = tempDir/"temp_exp_outdir"

proc editSyntrainerEnv(dcPath, storagePath: string, cmdToStartDC: string) =
    var buffer = ""
    var envFile = makeAbsolute("syntrainer.env")
    for line in envFile.lines:
        if $EnvVarIdentifier.dcPath & "=" in line:
            buffer &= $EnvVarIdentifier.dcPath & "=\"" & dcPath & "\"\n"
        elif $EnvVarIdentifier.storagePath & "=" in line:
            buffer &= $EnvVarIdentifier.storagePath & "=\"" & tempStoragePath & "\"\n"
        elif $EnvVarIdentifier.commandString & "=" in line:
            buffer &= $EnvVarIdentifier.commandString & "=\"" & cmdToStartDC & "\"\n"
        elif $EnvVarIdentifier.dcAdapterOutputOnConsole & "=" in line:
            buffer &= $EnvVarIdentifier.commandString & "=FALSE\n"
        else:
            buffer &= line & '\n'
    writeFile(envFile, buffer)

proc printSolvedTasksWithNimCode(synResults: seq[SYNTranslationResult]) =
    for synResult in synResults:
        echo synResult.ident.taskName
        for line in synResult.translationResult:
            echo line
        echo ""

proc init() =
    assert paramCount() >= 0

    var argPosECPath = 1
    if paramCount() >= argPosECPath:
        echo "Using DreamCoder Installation"
        dirEC = paramStr(argPosECPath)
        echo dirEC
        useMockEC = false
    else:
        echo "Using DreamCoder-Mock"

    if useMockEC:
        editSyntrainerEnv(dirEC, sourceDir, "python3")
    else:
        editSyntrainerEnv(dirEC, sourceDir, "./container.img;python3")

    if os.fileExists(localSyndcadapter):
        os.removeFile(localSyndcadapter)

    os.copyFile(copyTestFiles/"syndcadapter_forTesting.py", localSyndcadapter)

    if os.dirExists(tempDir):
        os.removeDir(tempDir)
    os.createDir(tempDir)

    os.createDir(tempStoragePath)

    os.createDir(tempDirOut)

    if useMockEC:
        os.createDir(dirEC)
        os.createDir(dirEC/"compressionMessages")
        os.createDir(dirEC/"bin")
        os.copyFile(sourceDir/"syndcadapter_forTesting.py", dirEC/"bin"/"syndcadapter.py")

    # Init SYN-Trainer
    echo "Initializing SYN-Trainer ..."
    syntrainer.initialize()
    os.copyFileToDir(sourceDir/"setting_default.json", tempStoragePath/"setting_files")
    echo "SYN-Trainer was initialized."


proc findMatching20File(name: string, dir: string): string =
    let filesInPath = getAllFiles(dir)
    for fileName in filesInPath:
        if fileName in name:
            var name20 = filename
            #TODO: remove magic numbers
            name20.removeSuffix({'1', '0'})
            result = name20 & "20"

proc stretchToLength(str: string, length: int): string =
    if str.len >= length:
        return str
    else:
        return str & " ".repeat(length - str.len)


unittest.disableParamFiltering()

suite "1-Click-Demo":
    echo "This 1-Click-Demo demonstrates the functionality of the API."
    echo "Goal of this demo: "
    echo "1. is to synthesize all tb1_cases with DC."
    echo "2. Receiving for all solved tasks translatations in Nim-Code of the found programs."
    echo "3. Then we demonstrate our interpreter by determining the outputs of all input examples"
    echo "with the help of the found programs."
    echo "4. we asign SYN-Trainer the same tasks again, but this time we show that SYN-Trainer is aware of tb1cases (are cached)"
    echo "and return the programs immediatly \n"

    # Pre Init
    init()

    test "Create and Run tb1cases":
        echo "creating and running"
        var taskNamesRes = syntrainer.createAndRunTasksInNewExperiment(
                tb1cases.map((filename: string) => pathToCSVs/filename & "_10.csv"))

        check:
            taskNamesRes.isSome and tb1cases.len == taskNamesRes.get().len
            syntrainer.lenSolvedTasks() == 0

        taskNames = taskNamesRes.get()
    echo ""

    echo "The following tasks were created by SYN-Trainer:"
    echo $taskNames
    # Since we made no update call only known tasks are already solved
    echo "Number of cached tasks: " & $syntrainer.lenSolvedTasks()

    if useMockEC:
        echo "Mock: DreamCoder found solutions."
        mockSYNDCAdapterSolvedTask(dirEC, sourceDir, tb1cases, taskNames)
    else:
        echo "We give DreamCoder " & $waitSec & " seconds to solve the tasks."
        os.sleep(waitSec * 1000)
    echo ""

    test "Check for updates":
        var numberSolvedTasks = syntrainer.update()

        check:
            numberSolvedTasks > 0

        echo "These are the " & $numberSolvedTasks & "solved tasks: \n"

        while syntrainer.lenSolvedTasks() > 0:
            var solvedTaskRes = syntrainer.getSolvedTask()
            if solvedTaskRes.isSome:
                synResults.add(solvedTaskRes.get())
        printSolvedTasksWithNimCode(synResults)
    echo ""


    test "Test interpreter":
        echo "SYN-Trainer now determines the output by applying the solved program on input examples:"

        for result in synResults:
            var identifier = result.ident
            echo "comparing outputs generated by the solved program of  ", identifier.taskName, " with testdata \n"
            let matching20File = pathToCSVs / findMatching20File(identifier.taskName, pathToCSVs) & ".csv"
            let (input, output) = readValueTypes(matching20File)

            let detOutputsRes = getOutputsForSolvedTaskIdent(identifier, input)

            require:
                detOutputsRes.isSome

            let detOutputs = detOutputsRes.get()
            check:
                detOutputs == output

            for i in countup(0, output.len-1):
                echo stretchToLength(input[i].toStr, 20) &
                        " -> " & stretchToLength(detOutputs[i].toStr, 20) &
                        " =?= " & stretchToLength(output[i].toStr, 20)
            echo "\n"
    echo ""

    test "Test cache with tb1cases":
        echo "We assign SYN-Trainer the tb1cases again..."
        taskNames = @[]
        var taskNamesRes = syntrainer.createAndRunTasksInNewExperiment(
                tb1cases.map((filename: string) => pathToCSVs/filename & "_10.csv"))

        check:
            taskNamesRes.isSome
            tb1cases.len == taskNamesRes.get().len
            # all tasks should be solved immediatly
            syntrainer.lenSolvedTasks() == tb1cases.len

        taskNames = taskNamesRes.get()

        echo "SYN-Trainer created " & $syntrainer.lenSolvedTasks() & " tasks"

        synResults = @[]
        while syntrainer.lenSolvedTasks() > 0:
            var solvedTaskRes = syntrainer.getSolvedTask()
            if solvedTaskRes.isSome:
                synResults.add(solvedTaskRes.get())

        echo "found these results in cache: \n" & $synResults.map((res: SYNTranslationResult) => res.ident.taskName)

    echo ""
    syntrainer.close()
    os.removeFile(localSyndcadapter)
    os.removeDir(tempDir)
