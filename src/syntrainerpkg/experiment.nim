##    experiment.nim
##    defining Experiment type
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import tables
import exceptions, task, helpers

type Experiment* = ref object
    name*: string
    domain*: string
    tasks*: seq[Task]
    outdir*: string
    settings*: OrderedTable[string, string]

#Settings to check
let requiredIntSettings = @["enumerationTimeout", "iterations", "recognitionTimeout", "maximumFrontier",
                        "topK", "structurePenalty", "testingTimeout", "arity"]
let requiredFloatSettings = @["pseudoCounts", "helmholtzRatio"]
let optionalFloatSettings: seq[string] = @[]
let optionalIntSettings: seq[string] = @[]
let validDomains = @["text"]
let validActivations = @["tanh"]

let defaultSettings* = """
{
    "domain": "text",
    "outdir": "/path/to/outdir/of/experiment",
    "settings": {
        "enumerationTimeout": 10,
        "activation": "tanh",
        "iterations": 10,
        "recognitionTimeout": 3600,
        "maximumFrontier": 10,
        "topK": 2,
        "pseudoCounts": 30.0,
        "helmholtzRatio": 0.5,
        "structurePenalty": 1,
        "testingTimeout": 0,
        "arity": 3
    }
}"""

proc isDomainValid(domain: string): bool =
    #No other domains supported yet
    return domain in validDomains

proc isActivationValid(activation: string): bool =
    return activation in validActivations

proc checkNumericalSettings(exp: Experiment);
proc checkNumericalSettings(exp: Experiment, required: bool, settingsType: string);


proc checkSettings*(experiment: Experiment) =
    #if not os.dirExists(experiment.outdir.makeAbsolute()):
    #    raise newException(InvalidSettingsError, "Experiment '" & experiment.name & "' has invalid value for 'outdir'. (Path '"&experiment.outdir.makeAbsolute()&"' doesn't exist)")

    if not isDomainValid(experiment.domain):
        raise newException(InvalidSettingsError, "Experiment '" & experiment.name & "' has invalid value for 'domain'.")

    if experiment.settings.hasKey("activation"):
        if not isActivationValid(experiment.settings["activation"]):
            raise newException(InvalidSettingsError, "Experiment '" & experiment.name & "' has invalid value for 'activation'.")

    checkNumericalSettings(experiment)

proc checkNumericalSettings(exp: Experiment) =
    checkNumericalSettings(exp, true, "int")
    checkNumericalSettings(exp, false, "int")
    checkNumericalSettings(exp, true, "float")
    checkNumericalSettings(exp, false, "float")

proc checkNumericalSettings(exp: Experiment, required: bool, settingsType: string) =
    var settingsToCheck: seq[string]
    if required:
        if settingsType == "int":
            settingsToCheck = requiredIntSettings
        elif settingsType == "float":
            settingsToCheck = requiredFloatSettings
    else:
        if settingsType == "int":
            settingsToCheck = optionalIntSettings
        elif settingsType == "float":
            settingsToCheck = optionalFloatSettings

    for setting in settingsToCheck:
        if not exp.settings.hasKey(setting) and required:
            raise newException(InvalidSettingsError, "Experiment '" & exp.name & "' is missing setting '" & setting & "'")
        if not exp.settings.hasKey(setting) and not required:
            continue
        var valueToCheck = exp.settings[setting]
        if settingsType == "int" and not isInt(valueToCheck):
            raise newException(InvalidSettingsError, "Experiment '" & exp.name & "' has invalid value type for '"&setting&"'. (Integer expected)")
        if settingsType == "float" and not isFloat(valueToCheck):
            raise newException(InvalidSettingsError, "Experiment '" & exp.name & "' has invalid value type for '"&setting&"'. (Float expected)")

proc hasValidSettings*(experiment: Experiment): bool =
    try:
        experiment.checkSettings()
        return true
    except InvalidSettingsError:
        return false

proc `==`*(exp1: Experiment, exp2: Experiment): bool =
    if exp1.isNil and exp2.isNil:
        return true
    if cast[pointer](exp1) == cast[pointer](exp2):
        return true
    if exp1.isNil or exp2.isNil:
        return false
    if exp1.name != exp2.name:
        return false
    if exp1.domain != exp2.domain:
        return false
    if exp1.outdir != exp2.outdir:
        return false
    if exp1.settings != exp2.settings:
        return false
    if exp1.tasks.len != exp2.tasks.len:
        return false
    for index in 0..<exp1.tasks.len:
        if exp1.tasks[index] != exp2.tasks[index]:
            return false
    return true

proc `!=`*(exp1: Experiment, exp2: Experiment): bool =
    return not (exp1 == exp2)

##Call "experiment$<int>" to give the second argument
proc `$`*(experiment: Experiment, tasksToShow: int = 3): string =

    if experiment == nil:
        return "Called $experiment but got nil as argument."
    result = "Exp-Name: " & experiment.name
    let tasksCount = experiment.tasks.len()
    let limit =  min(tasksCount, tasksToShow)
    for i in 0..<limit:
        result &= "\n\tTask " & $i & ": " & $experiment.tasks[i].name
    if limit < tasksCount:
        result &= "\t... (" & `$`(tasksCount-limit) & " not displayed)"
