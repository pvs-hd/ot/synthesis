##    text.nim
##    Contains primitives and helpers for the text domaine
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import sugar, sequtils, strutils, tables

import ../types
import ../domain_utility

# primitives for the interpreter
defineCallablePrimitive(ec_chr2str, cVal, ecChar, ecString):
    $cVal.charVal

defineCallablePrimitive(ec_concatenate, word1Val, ecString, ecCallable):
    newCallable(word2Val, ecString, ecString):
        word1Val.toStr() & word2Val.toStr()

defineCallablePrimitive(ec_nthChar, nVal, ecInt, ecCallable):
    newCallable(wordVal, ecString, ecChar):
        char(toStr(wordVal)[toInt(nVal)])

defineCallablePrimitive(ec_upper, wordVal, ecString, ecString):
    toStr(wordVal).toUpper()

defineCallablePrimitive(ec_lower, wordVal, ecString, ecString):
    toStr(wordVal).toLower()

defineCallablePrimitive(ec_nthString, nVal, ecInt, ecCallable):
    newCallable(seqVal, ecSeq, ecString):
        # TODO ecProgramAssert(toSeq(seqVal).len > toInt(nVal), "index out of bound")
        toStr(toSeq(seqVal)[toInt(nVal)])

defineCallablePrimitive(ec_split, delimiterVal, ecChar, ecCallable):
    newCallable(wordVal, ecString, ecSeq):
        map(toStr(wordVal).split(toChar(delimiterVal)), toValueType)

defineCallablePrimitive(ec_sliceString, iVal, ecInt, ecCallable):
    newCallable(jVal, ecInt, ecCallable):
        newCallable(wordVal, ecString, ecString):
            toStr(wordVal)[toInt(iVal)..toInt(jVal)-1]

defineCallablePrimitive(ec_prefix, iVal, ecInt, ecCallable):
    newCallable(wordVal, ecString, ecString):
        toStr(wordVal)[0..toInt(iVal)-1]

defineCallablePrimitive(ec_firstPrefix, wordVal, ecString, ecString):
    toStr(wordVal)[0]

defineCallablePrimitive(ec_incr, iVal, ecInt, ecInt):
    toInt(iVal)+1

defineConstPrimitive(ec_0, ecInt):
    0

defineConstPrimitive(ec_1, ecInt):
    1

defineConstPrimitive(ec_2, ecInt):
    2

defineConstPrimitive(ec_3, ecInt):
    3

defineConstPrimitive(ec_4, ecInt):
    4

defineConstPrimitive(ec_comma, ecChar):
    ','

defineConstPrimitive(ec_dot, ecChar):
    '.'

defineConstPrimitive(ec_space, ecChar):
    ' '

defineConstPrimitive(ec_at, ecChar):
    '@'

defineConstPrimitive(ec_minus, ecChar):
    '-'

defineConstPrimitive(ec_lparen, ecChar):
    '('

defineConstPrimitive(ec_rparen, ecChar):
    ')'

# every primitive is a lambda has to fulfill the signature of Callable
# even if the parameter is not used


proc ec_map(mapper: ValueType): ValueType =
    assert mapper.kind == ecCallable
    result = ValueType(kind: ecCallable, callableVal: (wordsVal: ValueType) => (
        var newWords: seq[ValueType] = @[];
        for wordVal in toSeq(wordsVal): newWords.add(mapper.callableVal(wordVal));
        return ValueType(kind: ecSeq, seqVal: newWords)
    ))

proc ec_first(wordsVal: ValueType): ValueType =
    ValueType(kind: ecSeq, seqVal: @[wordsVal.seqVal[0]])

let textPrimitives* = {
    "0": Primitive(name: "0", isConstant: true, callable: ec_0),
    "1": Primitive(name: "1", isConstant: true, callable: ec_1),
    "2": Primitive(name: "2", isConstant: true, callable: ec_2),
    "3": Primitive(name: "3", isConstant: true, callable: ec_3),
    "4": Primitive(name: "4", isConstant: true, callable: ec_4),
    "\',\'": Primitive(name: "\',\'", isConstant: true, callable: ec_comma),
    "\'.\'": Primitive(name: "\'.\'", isConstant: true, callable: ec_dot),
    "SPACE": Primitive(name: "SPACE", isConstant: true, callable: ec_space),
    "LPAREN": Primitive(name: "LPAREN", isConstant: true, callable: ec_lparen),
    "RPAREN": Primitive(name: "RPAREN", isConstant: true, callable: ec_rparen),
    "\'@\'": Primitive(name: "\'@\'", isConstant: true, callable: ec_at),
    "\'-\'": Primitive(name: "\'-\'", isConstant: true, callable: ec_minus),
    "upper": Primitive(name: "upper", isConstant: false, callable: ec_upper),
    "lower": Primitive(name: "lower", isConstant: false, callable: ec_lower),
    "concatenate": Primitive(name: "concatenate", isConstant: false, callable: ec_concatenate),
    "chr2str": Primitive(name: "chr2str", isConstant: false, callable: ec_chr2str),
    "nthChar": Primitive(name: "nthChar", isConstant: false, callable: ec_nthChar),
    "sliceString": Primitive(name: "sliceString", isConstant: false, callable: ec_sliceString),
    "map": Primitive(name: "map", isConstant: false, callable: ec_map),
    "first": Primitive(name: "first", isConstant: false, callable: ec_first),
    "nthString": Primitive(name: "nthString", isConstant: false, callable: ec_nthString),
    "split": Primitive(name: "split", isConstant: false, callable: ec_split),
    "prefix": Primitive(name: "prefix", isConstant: false, callable: ec_prefix),
    "firstPrefix": Primitive(name: "firstPrefix", isConstant: false, callable: ec_firstPrefix),
    "incr": Primitive(name: "incr", isConstant: false, callable: ec_incr),
}.newTable
