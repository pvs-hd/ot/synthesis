##    compile_text.nim
##    contains primitives of the textdomain
##    this file should be imported to compile translated nim code
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import sugar, strutils

# constant primitives do not have to be added
proc upper*(word: string): string = toUpper(word)
proc lower*(word: string): string = toLower(word)
proc concatenate*(word1: string): ((string) -> string) = (word2: string) => word1 & word2
proc chr2str*(c: char): string = $c # we interprete already chars as strings in nim
proc nthChar*(n: int): ((string) -> char) = (word: string) => word[n]
proc sliceString*(i: int): ((int) -> ((string) -> string)) =
  (j: int) => (
    (word: string) => word[i..j])
proc nthString*(n: int): ((seq[string]) -> string) = (wordlist: seq[string]) => wordlist[n]
proc split*(c: char): ((string) -> seq[string]) = (word: string) => word.split(c)
proc prefix*(i: int): ((string) -> string) = (word: string) => word[0..i]
proc firstPrefix*(word: string): string = $word[0]
proc incr*(n: int): int = n+1
