##    controller.nim
##    creates and deletes experiments and tasks for DC
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz
import os, options, strutils, sequtils, times
import experiment, task, storage, exceptions, environment_variables
import syndcadapter_starter, types, translation, cache, interpreter

# controller logic
# general system functions
proc runExperiment*(experimentName: string);
proc createTask*(name: string, pathToDataCSV: string): Task;
proc createTasks*(pathToDataCSVs: seq[string]): seq[Task];
proc saveTask*(task: Task): Task;
proc importECProgramsOfSolvedTasks*();
# only CLI sepcific system functions
proc listExperiments*(): seq[string];
proc listTasks*(): seq[string];
proc createAndSaveTask*(name: string, pathToDataCSV: string): Option[Task];
proc createAndSaveExperiment*(name : string, settingFile: string, taskNames: seq[string]): Option[Experiment];
proc deleteTask*(taskName: string);
proc deleteExperiment*(experimentName: string);
proc getTranslatedECProgramOfSolvedTask*(taskName : string): Option[FormattedCodeLines];
# only API (using cache) sepecific system functions
proc getSYNTranslationResults*(taskName: string): seq[SYNTranslationResult];
proc getSYNTranslationResults*(ioDatas: seq[IOData]): seq[SYNTranslationResult];
proc getOutputsForSolvedTaskIdent*(ident: SYNResultIdent, inputs: seq[ValueType]): Option[seq[ValueType]];
proc createAndSaveExperimentForTasks*(taskNames: seq[string]): Option[Experiment]

#Helpers:
proc getAllTaskNames(): seq[string] =
    storage.getAllTaskNames()

proc getAllExperimentNames(): seq[string] =
    storage.getAllExperimentNames()

proc findTasksByNames(taskNames: seq[string]): seq[Task];

proc findTasksByNames(taskNames: seq[string]): seq[Task] =
    for taskName in taskNames:
        var task = storage.getTask(taskName)
        result.add(task)

proc createUniqueExperimentName*(ident: string) : string =
    var expNumber = cpuTime()
    var expPrefix = "exp_" & ident & "_"
    result = expPrefix & ($(expNumber*100000)).split(".")[0]
    var allExpNames = storage.getAllExperimentNames()
    assert not (result in allExpNames)

proc createUniqueTaskName*(ident: string) : string =
    let taskNumber = cpuTime()
    var taskPrefix = "task_" & ident & "_"
    result = taskPrefix & ($(taskNumber*100000)).split(".")[0]
    var allTaskNames = storage.getAllTaskNames()
    assert not (result in allTaskNames)

proc toSYNTranslationResult(synProgramResult: SYNProgramResult): SYNTranslationResult =
    result = SYNTranslationResult(ident: synProgramResult.ident,
        translationResult: translation.translateProgramTreeToNim(synProgramResult.programResult)
    )
#End: Helpers

proc runExperiment*(experimentName: string) =
    ## Starts a process of the `syndcadapter.py` in the current DreamCoder installation
    ## for the given experiment.
    ## Takes the command from the .env file.
    let experiment = storage.getExperiment(experimentName)

    experiment.checkSettings()

    let command = os.getEnv($commandString)

    startSynDcAdapter(experimentName, command)

proc listExperiments*(): seq[string] =
    ## Returns all experiment names found in the current storage location.
    getAllExperimentNames()

proc listTasks*(): seq[string] =
    ## Returns all task names found in the current storage location.
    getAllTaskNames()

proc createAndSaveTask*(name: string, pathToDataCSV: string): Option[Task] =
    ## Creates a task for a given name and given data (path to csv file) and saves it
    ## in the storage. It is nessecary that the name was not in use before.

    var task = createTask(name, pathToDataCSV)

    if storage.save(task):
        result = some(task)
    else:
        result = none(Task)

proc createTask*(name: string, pathToDataCSV: string): Task =
    ## It creates only a task. It can later be saved.
    if name in getAllTaskNames():
        raise newException(InvalidUserInputError, "Task '" & name & "' already exists.")
    result = task.newTask(name, pathToDataCSV)

proc createTasks*(pathToDataCSVs: seq[string]): seq[Task] =
    ## Creates a task with an unique name for given data. The task is not stored yet.
    for path in pathToDataCSVs:
        var name = createUniqueTaskName(splitFile(path).name)
        result.add(createTask(name, path))

proc saveTask*(task: Task): Task =
    ## Saves a task.
    discard storage.save(task)
    result = task

proc deleteTask*(taskName: string) =
    ## Delete a task by a given name.
    if not storage.deleteTask(taskName):
        raise newException(InvalidUserInputError, "Task '" & taskName & "' couldn't be found.")

proc deleteExperiment*(experimentName: string) =
    ## Delete an experiment by a given name.
    if not storage.deleteExperiment(experimentName):
        raise newException(InvalidUserInputError, "Experiment '" & experimentName & "' couldn't be found.")

proc createAndSaveExperiment*(name : string, settingFile: string, taskNames: seq[string]): Option[Experiment] =
    if name in getAllExperimentNames():
        raise newException(InvalidUserInputError, "Experiment '" & name & "' already exists.")

    var tasks = findTasksByNames(taskNames)

    var experiment = Experiment(name: name, tasks: tasks)

    storage.addSettings(experiment, settingFile)

    if storage.save(experiment):
        return some(experiment)
    else:
        return none(Experiment)

proc createAndSaveExperimentForTasks*(taskNames: seq[string]): Option[Experiment] =
    # return experiment name if creaed and saved
    var settingFile = "setting_default" # TODO: get from env / or some other constant
    var someIdentifier = "auto_generated" # some string used for name
    var experimentName = createUniqueExperimentName(someIdentifier)

    result = createAndSaveExperiment(experimentName, settingFile, taskNames)

proc importECProgramsOfSolvedTasks*() =
    # copies all ec program of all found compression messages
    # into storage/solved st. they can be translated

    for compressionMessagePath in storage.getCompressionMessagePaths():
        discard storage.saveECPrograms(compressionMessagePath, false)
    cache.update()

proc getTranslatedECProgramOfSolvedTask*(taskName : string): Option[FormattedCodeLines] =
    ## returns the translated program using the first ec program
    ## this proc should only be used in an CLI sencario since it does not
    ## consider to search in the cache
    var ecPrograms: seq[string] = @[]

    try:
        ecPrograms = storage.getAllECProgramsForSolvedTask(taskName)
    except NotFoundInStorageError:
        return none(FormattedCodeLines)

    const solutionNumber = 0;

    if ecPrograms.len()-1 < solutionNumber:
        return none(FormattedCodeLines)

    var expression: Expression = storage.getAllECProgramsForSolvedTask(taskName)[solutionNumber]
    var translation = translateECProgramStringToNim(expression)
    result = some(translation)

proc getSYNTranslationResults*(taskName: string): seq[SYNTranslationResult] =
    ## tries to find a solution of task in cache and returns its translation as SYNTranslationResult
    result = cache.findSolvedTaskByTaskName(taskName).map(toSYNTranslationResult)

proc getSYNTranslationResults*(ioDatas: seq[IOData]): seq[SYNTranslationResult] =
    result = cache.findSolvedTaskByIOExamples(ioDatas).map(toSYNTranslationResult)

proc getOutputsForSolvedTaskIdent*(ident: SYNResultIdent, inputs: seq[ValueType]): Option[seq[ValueType]] =
    var synProgramRes = cache.getSYNProgramResultBySYNResultIdent(ident)
    if synProgramRes.isSome():
        var program = Abstraction(synProgramRes.get().programResult)
        var res: seq[ValueType] = @[]
        for input in inputs:
            var outputRes = interpreter.call(program, input)
            if outputRes.isSome:
                res.add(outputRes.get())
            else:
                res.add(ValueType(kind: ecNone))
        return some(res)
    else:
        return none(seq[ValueType])
