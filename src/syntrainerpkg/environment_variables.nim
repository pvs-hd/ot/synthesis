##    environment_variables.nim
##    defining environmental variables
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import strformat

type EnvVarIdentifier* = enum
    storagePath = "STORAGEPATH"
    dcAdapterOutputOnConsole = "WRITE_DCADAPTER_ON_CONSOLE"
    storageType = "STORAGETYPE"
    jsonPrettyPrinting = "JSON_PRETTY_PRINTING"
    commandString = "COMMAND_STRING"
    dcPath = "DCPATH"

let defaultEnviromentVariables* = fmt("""
{storagePath}="../syntrainer_data/experiments"
{dcAdapterOutputOnConsole}=FALSE
{storageType}=JSON
{jsonPrettyPrinting}=TRUE
{commandString}="python"
{dcPath}="../ec"
""")
