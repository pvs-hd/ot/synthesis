##    translation.nim
##    This module defines procedures for translating
##    EC programs (programs found by DreamCoder) to NimCode
##    A lot of code is inspired by the DreamCoder project.
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz


import sugar, strutils, strformat, sequtils, parseutils, tables
import types # TODO: exception handling
from domains import primitives

proc parseECProgramStringToTree*(ecProgram: Expression): ProgramFragment;
proc translateProgramTreeToNim*(programTree: ProgramFragment): FormattedCodeLines;
proc translateECProgramStringToNim*(ecProgram: Expression): FormattedCodeLines;

##    helpers for printing and debugging
proc `$`(parseType: ExpressionNode): string =
    if len(parseType.children) == 0:
        result = parseType.ident
    else:
        result = "[" & parseType.ident
        for child in parseType.children:
            result &= $child & " "
        result &= "]"

proc `$`*(fragment: ProgramFragment): string =
    if fragment of Abstraction:
        let abstraction = Abstraction(fragment)
        return "Abstraction(identifier: " & abstraction.identifier & ", body: " & "@[" & $abstraction.body & "]" & ")"
    if fragment of Application:
        let application = Application(fragment)
        return "Application(program: " & $application.program & ", input: " & $application.input & ")"
    if fragment of Primitive:
        return "Primitive(" & "name: " & Primitive(fragment).name & ")"
    if fragment of Parameter:
        return "Parameter(" & "index: " & Parameter(fragment).index & ")"

proc parseECProgramStringToExpTree(ecProgram: Expression): auto =
    ## ``parseECProgramStringToExpTree`` reads in a ecProgram as a string
    ## and parses it into a tree of ExpressionNodes to establish the structure
    proc parseHashSign(parsePos: int): (ExpressionNode, int);
    proc parseBrackets(parsePos: int): (ExpressionNode, int);
    proc parseWords(parsePos: int): (ExpressionNode, int);
    var ecProgram = strip(ecProgram)

    proc atIndexThereIs(parsePos: int, sign: char): bool =
        return ecProgram[parsePos] == sign

    proc regularWhitespace(parsePos: int): bool =
        return parsePos <= len(ecProgram) and isspaceAscii(ecProgram[parsePos])

    proc regularNonWhitespaceInsideBrackets(parsePos: int): bool =
        return parsePos < len(ecProgram) and
            not isspaceAscii(ecProgram[parsePos]) and
            $ecProgram[parsePos] != "(" and
            $ecProgram[parsePos] != ")"

    proc bracketClosed(parsePos: int): bool =
        return $ecProgram[parsePos] == ")"

    proc firstNonWhitespaceFrom(parsePos: int): int =
        var parsePos = parsePos
        while regularWhitespace(parsePos):
            parsePos += 1
        return parsePos

    proc translateFromIndex(parsePos: int): (ExpressionNode, int) =
        var parsePos = firstNonWhitespaceFrom(parsePos)

        if parsePos == len(ecProgram):
            echo "parse failure"

        # parse expressions starting with '#'
        elif atIndexThereIs(parsePos, '#'):
            return parseHashSign(parsePos)

        # parse function call/new lambda, i.e. (f_name arg1 arg2 ...)
        elif atIndexThereIs(parsePos, '('):
            return parseBrackets(parsePos)

        # if none of the above, parse raw words, i.e. primitive names, values
        else:
            return parseWords(parsePos)

    proc parseHashSign(parsePos: int): (ExpressionNode, int) =
        var (expressions, parsePos) = translateFromIndex(parsePos + 1)
        let hashExpressionNode: ExpressionNode = ExpressionNode(ident: "#", children: @[])
        var childs: seq[ExpressionNode] = concat(@[hashExpressionNode], @[expressions])
        return (ExpressionNode(ident: "", children: childs), parsePos)

    proc parseWords(parsePos: int): (ExpressionNode, int) =
        var word: string
        var parsePos = parsePos
        while regularNonWhitespaceInsideBrackets(parsePos):
            word.add(ecProgram[parsePos])
            parsePos += 1
        return (ExpressionNode(ident: word, children: @[]), parsePos)

    proc parseBrackets(parsePos: int): (ExpressionNode, int) =
        var expressionsInBracket: seq[ExpressionNode]
        var expressions: ExpressionNode
        var parsePos = parsePos + 1
        while true:
            (expressions, parsePos) = translateFromIndex(parsePos)
            expressionsInBracket.add(expressions)
            parsePos = firstNonWhitespaceFrom(parsePos)

            if parsePos == len(ecProgram):
                echo "parse failure"

            if bracketClosed(parsePos):
                parsePos += 1
                break
        return (ExpressionNode(ident: "", children: expressionsInBracket), parsePos)

    var startingIndex: int = 0
    let (structuredProgram, parsePos) = translateFromIndex(startingIndex)
    if parsePos == len(ecProgram):
        return structuredProgram
    echo "parse failure"


proc parseExpTreeToProgramFragment(expNode: ExpressionNode, abstractionCounter: int): (ProgramFragment, int) =
    ## ``parseExpTreeToProgramFragment()`` parses from a tree of ``ExpressionNodes`` to
    ## a tree of different program Fragments.
    ## "lambda" is keyword for ``Abstraction``
    ## evaluated programs are ``Application``
    ## ``Primitives`` need to be predefined, otherwise they will not be recognised
    ## ``Parameters`` are recognised by a leading "$" followed by an Index
    ## the "#" sign will not have any effect on the outcome, it is just a signal for DC
    if len(expNode.children) != 0:

        if expNode.children[0].ident == "#":
            assert len(expNode.children) == 2
            return parseExpTreeToProgramFragment(expNode.children[1], abstractionCounter)

        if expNode.children[0].ident == "lambda":
            assert len(expNode.children) == 2
            var (body, abstractionCounter) = parseExpTreeToProgramFragment(expNode.children[1], abstractionCounter)
            return (ProgramFragment(Abstraction(identifier: $(abstractionCounter + 1), body: body)), abstractionCounter + 1)
        # if not # or lambda it is an application chain of at least primitives

        var (program, abstractionCounter) = parseExpTreeToProgramFragment(expNode.children[0], abstractionCounter)
        for expression in expNode.children[1..^1]:
            var programInput: ProgramFragment
            (programInput, abstractionCounter) = parseExpTreeToProgramFragment(expression, abstractionCounter)
            program = ProgramFragment(Application(program: program, input: programInput))
        return (program, abstractionCounter)

    assert len(expNode.children) == 0
    var exp = expNode
    if exp.ident[0] == '$':
        var index: int
        discard parseInt(exp.ident, index, 1)
        return (ProgramFragment(Parameter(index: $index)), abstractionCounter)

    if primitives.hasKey(exp.ident):
        return (ProgramFragment(primitives[exp.ident]), abstractionCounter)

#predefinition, helpers
proc callerName(program: Abstraction): Expression = fmt"lambda{program.identifier}"
proc paramName(program: Abstraction): Expression = fmt"param{program.identifier}"

method translateToNim(fragment: ProgramFragment, paramNames: var seq[Expression]): (FormattedCodeLines, Expression) {. base, locks: "unknown" .} = discard
method translateToNim(fragment: Abstraction, paramNames: var seq[Expression]): (FormattedCodeLines, Expression) {. locks: "unknown" .};
method translateToNim(fragment: Application, paramNames: var seq[Expression]): (FormattedCodeLines, Expression) {. locks: "unknown" .};
method translateToNim(fragment: Primitive, paramNames: var seq[Expression]): (FormattedCodeLines, Expression) {. locks: "unknown" .};
method translateToNim(fragment: Parameter, paramNames: var seq[Expression]): (FormattedCodeLines, Expression) {. locks: "unknown" .};

method translateToNim(fragment: Abstraction, paramNames: var seq[Expression]): (FormattedCodeLines, Expression) =
    # translate an abstraction
    paramNames = @[fragment.param_name] & paramNames

    var (body_translation, return_translation) = fragment.body.translateToNim(paramNames)
    const tab = "   "
    let translation = concat(@[
            fmt"proc {fragment.callerName}({fragment.paramName}: auto): auto =",
            fmt"   var {fragment.paramName} = {fragment.paramName}"
            ],
            body_translation.map(x => tab & x),
            @[tab & "return " & return_translation])
    return (translation, "lambda" & fragment.identifier)

method translateToNim(fragment: Application, paramNames: var seq[Expression]): (FormattedCodeLines, Expression) =
    # translate an application
    var (abstractionTranslation, inputTranslation) = fragment.input.translateToNim(paramNames)

    var callTranslation: Expression

    let (abstractionTranslationProgram, callerTranslation) = fragment.program.translateToNim(paramNames)
    abstractionTranslation = concat(abstractionTranslation, abstractionTranslationProgram)
    callTranslation = callerTranslation & "(" & inputTranslation & ")"

    return (abstractionTranslation, callTranslation)

method translateToNim(fragment: Primitive, paramNames: var seq[Expression]): (FormattedCodeLines, Expression) =
    # translate an primitive
    var emptySequence: FormattedCodeLines = @[]
    return (emptySequence, $fragment.name)

method translateToNim(fragment: Parameter, paramNames: var seq[Expression]): (FormattedCodeLines, Expression) =
    # translate an parameter
    var emptySequence: FormattedCodeLines = @[]
    var returnIndex: int
    discard parseInt(fragment.index, returnIndex, 0)
    return (emptySequence, paramNames[returnIndex])

proc parseECProgramStringToTree*(ecProgram: Expression): ProgramFragment =
    # parse ec program (formatted as lisp string) to a tree of program fragments
    let expressionTree: ExpressionNode = parseECProgramStringToExpTree(ecProgram)
    let (programTree, _) = parseExpTreeToProgramFragment(expressionTree, 0)
    result = programTree

proc translateProgramTreeToNim*(programTree: ProgramFragment): FormattedCodeLines =
    # translate program tree to nim code
    var paramNames: FormattedCodeLines = @[]
    var (translation, _) = translateToNim(programTree, paramNames)
    result = translation

proc translateECProgramStringToNim*(ecProgram: Expression): FormattedCodeLines =
    # translate an lisp formatted ec program as string to nim code
    var programTree = parseECProgramStringToTree(ecProgram)
    result = translateProgramTreeToNim(programTree)
