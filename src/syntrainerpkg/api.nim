##    api.nim
##    API for CORE to use main functionalty of SYN-Trainer
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz
import sequtils, options, deques
import controller, exceptions, types, cache, task, experiment, syndcadapter_starter

import os
import dotenv
import cli
import storage
import environment_variables

# constants
const version       = "0.3.0"
const envFileName   = "syntrainer.env"
let envPath         = os.getAppDir()


export types

# API
proc initialize*(startCLI: bool = false);
proc update*(): int;
proc createAndRunTasksInNewExperiment*(pathToDataCSVs: seq[string]): Option[seq[string]];
proc getSolvedTask*(): Option[SYNTranslationResult];
proc getOutputsForSolvedTaskIdent*(ident: SYNResultIdent, inputs: seq[ValueType]): Option[seq[ValueType]];
proc lenSolvedTasks*(): int;


# API-helpers
proc updateActiveTasks();
proc chooseSYNTranslationResult(results: seq[SYNTranslationResult]): SYNTranslatioNResult;

# FormattedCodeLines is an alias of seq[string]
var solvedTasksQueue = initDeque[SYNTranslationResult]()
var activeTasks: seq[string] = @[]

# API
proc initialize*(startCLI = false) =
    ## initialize SYN-Trainer, especially checks for the storage and initiales enviromental variables
    ## usage: this procedure should be called before calling any other procedure of this api

    # set default environment variables
    loadEnvFromString(defaultEnviromentVariables)

    # overload environment variables from .env
    if os.fileExists(envPath/envFileName):
        let env = initDotEnv(envPath, envFileName)
        env.overload()
    else:
        echo("Warning: " & envFileName & " file not found at " & envPath/envFileName)
        echo("Loaded default environment variables: \n" & defaultEnviromentVariables)


    # init storage
    try:
        storage.initStorage()
    except StorageError:
        echo(getCurrentExceptionMsg())
        return
    except:
        echo(repr(getCurrentException()))
        return

    # run CLI for command line arguments
    if startCLI == true:
        cli.run(os.commandLineParams(), version)
    else:
        # cache is only used for the API
        cache.init()

proc update*(): int =
    ## update active tasks
    ## usage: this procedure should be called frecuently before checking for new solved tasks
    ## returns the number of solved tasks

    if len(activeTasks) == 0:
        return 0

    controller.importECProgramsOfSolvedTasks()

    updateActiveTasks()

    result = len(solvedTasksQueue)

proc createAndRunTasksInNewExperiment*(pathToDataCSVs: seq[string]): Option[seq[string]] =
    ## creates tasks if the tasks were not solved yet
    ## if one task was already solved, the result for this special task is returned immediatly
    ## otherwise the synthsized programs can be received later with getSolvedTask(...)
    if len(pathToDataCSVs) == 0:
        return none(seq[string])

    var newActiveTaskNames : seq[string] = @[]
    var newTaskNames : seq[string] = @[]

    var tasks = createTasks(pathToDataCSVs)
    for task in tasks:
        var results = getSYNTranslationResults(task.data)
        if len(results) > 0:
            var cachedResult = chooseSynTranslationResult(results)
            solvedTasksQueue.addLast(cachedResult)
            newTaskNames.add(cachedResult.ident.taskName)
        else:
            discard saveTask(task)
            newActiveTaskNames.add(task.name)
            newTaskNames.add(task.name)

    var experimentRes = none(Experiment)
    if len(newActiveTaskNames) > 0:
        experimentRes = createAndSaveExperimentForTasks(newActiveTaskNames)

    if experimentRes.isSome:
        runExperiment(experimentRes.get().name)
    else:
        result = none(seq[string])

    activeTasks = concat(activeTasks, newActiveTaskNames)
    result = some(newTaskNames)

proc getSolvedTask*(): Option[SYNTranslationResult] =
    # try to get the last solved task with the result
    if len(solvedTasksQueue) != 0:
        result = some(solvedTasksQueue.popFirst)
    else:
        result = none(SYNTranslationResult)

proc getOutputsForSolvedTaskIdent*(ident: SYNResultIdent, inputs: seq[ValueType]): Option[seq[ValueType]] =
    result = controller.getOutputsForSolvedTaskIdent(ident, inputs)

proc lenSolvedTasks*(): int =
    result = len(solvedTasksQueue)

proc close*() =
    syndcadapter_starter.waitForExitOfAllProcesses()

# API-helpers
proc chooseSYNTranslationResult(results: seq[SYNTranslationResult]): SYNTranslatioNResult =
    # in general a solved task can have multiple found ec programs
    # we choose (to make this api easy) only the first program
    return results[0]

proc updateActiveTasks() =
    var toDelete: seq[int]

    for i in countup(0, len(activeTasks)-1):
        var synTranslationRes = controller.getSYNTranslationResults(activeTasks[i])
        if len(synTranslationRes) > 0:
            solvedTasksQueue.addLast(chooseSYNTranslationResult(synTranslationRes))
            toDelete.add(i)

    # update active tasks
    var countDels = 0
    for i in toDelete:
        activeTasks.del(i-countDels)
        countDels += 1
