##    storage.nim
##    logic for storing tasks and experiments
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import os, json, strutils, system
import json_storage, experiment, task, exceptions, environment_variables, helpers, types

proc initStorage*();
proc getTask*(name: string): Task;
proc getExperiment*(name: string): Experiment;
proc getAllTaskNames*():seq[string];
proc getAllExperimentNames*():seq[string];
proc getAllECProgramsForSolvedTask*(taskName: string): seq[Expression];
proc deleteTask*(name: string):bool;
proc deleteExperiment*(name: string):bool;
proc save*(experiment: Experiment): bool;
proc save*(task: Task): bool;
proc saveECPrograms*(compressionMessagePath: string, deleteAfterwards = true): bool;
proc addSettings*(experiment: var Experiment, settingsFile: string);

type
    StorageType = enum
        JSON
    StorageInfo = object
        case storageKind: StorageType
        of JSON:
            storagePath: string
            tasksSubdir: string
            ecProgramsSubdir: string
            settingsSubDir: string
            prettyPrinting: bool
            fileExtension: string

var storageInfo: StorageInfo
const TASKSUBDIR = "tasks"
const ECPROGRAMSSUBDIR = "solved"
const SETTINGSSUBDIR = "setting_files"
const FILEEXTENSION = ".json"

#Helpers
proc getTasksStoragePath(storageInfo: StorageInfo): string =
    storageInfo.storagePath/storageInfo.tasksSubdir

proc getECProgramsStoragePath(storageInfo: StorageInfo): string =
    storageInfo.storagePath/storageInfo.ecProgramsSubdir

proc getSettingsStoragePath(storageInfo: StorageInfo): string =
    storageInfo.storagePath/storageInfo.settingsSubdir

proc getExpStoragePath(storageInfo: StorageInfo): string =
    storageInfo.storagePath

proc getTaskPath(storageInfo: StorageInfo, taskName: string): string =
    getTasksStoragePath(storageInfo)/taskName&storageInfo.fileExtension

proc getExpPath(storageInfo: StorageInfo, expName: string): string =
    getExpStoragePath(storageInfo)/expName&storageInfo.fileExtension

proc getECProgramsPath(storageInfo: StorageInfo, taskName: string): string =
    getECProgramsStoragePath(storageInfo)/taskName&storageInfo.fileExtension

proc getSettingPath(storageInfo: StorageInfo, settingName: string): string =
    getSettingsStoragePath(storageInfo)/settingName&storageInfo.fileExtension
#End: Helpers

template tryStorageOperation(body : typed) =
    try:
        body
    except NotFoundInStorageError, JsonParsingError:
        raise newException(StorageError, getCurrentExceptionMsg())

proc getTask*(name: string): Task =
    case storageInfo.storageKind:
    of JSON:
        tryStorageOperation:
            json_storage.loadTask(storageInfo.getTaskPath(name))


proc getExperiment*(name: string): Experiment =
    case storageInfo.storageKind:
    of JSON:
        tryStorageOperation:
            result = json_storage.loadExperiment(storageInfo.getExpPath(name), storageInfo.getTasksStoragePath())


proc addSettings*(experiment: var Experiment, settingsFile: string)=
    case storageInfo.storageKind:
    of JSON:
        tryStorageOperation:
            json_storage.loadSettings(experiment, storageInfo.getSettingPath(settingsFile))

proc deleteTask*(name: string):bool =
    case storageInfo.storageKind:
    of JSON:
        try:
            json_storage.deleteTask(storageInfo.getTaskPath(name))
        except NotFoundInStorageError:
            return false
    return true

proc deleteExperiment*(name: string):bool =
    case storageInfo.storageKind:
    of JSON:
        try:
            json_storage.deleteExperiment(storageInfo.getExpPath(name))
        except NotFoundInStorageError:
            return false
    return true

proc getAllTaskNames*():seq[string] =
    case storageInfo.storageKind:
    of JSON:
        json_storage.getAllJsonFiles(storageInfo.getTasksStoragePath())

proc getAllSolvedTaskNames*():seq[string] =
    case storageInfo.storageKind:
    of JSON:
        json_storage.getAllJsonFiles(storageInfo.getECProgramsStoragePath())

proc getAllExperimentNames*():seq[string] =
    case storageInfo.storageKind:
    of JSON:
        json_storage.getAllJsonFiles(storageInfo.getExpStoragePath())

proc getAllECProgramsForSolvedTask*(taskName: string): seq[Expression] =
    case storageInfo.storageKind:
    of JSON:
        result = json_storage.loadECPrograms(storageInfo.getECProgramsPath(taskName))

proc save*(experiment: Experiment): bool =
    case storageInfo.storageKind:
    of JSON:
        try:
            json_storage.saveExperiment(experiment, storageInfo.getExpStoragePath(), storageInfo.prettyPrinting)
        except JsonParsingError:
            echo("Saving experiment '" & experiment.name & "' failed: " & repr(getCurrentException()))
            return false
    return true

proc save*(task: Task): bool =
    case storageInfo.storageKind:
    of JSON:
        try:
            json_storage.saveTask(task, storageInfo.getTasksStoragePath(), storageInfo.prettyPrinting)
        except JsonParsingError:
            echo("Saving task '" & task.name & "' failed: " & repr(getCurrentException()))
            return false
    return true

proc saveECPrograms*(compressionMessagePath: string, deleteAfterwards = true): bool =
    case storageInfo.storageKind:
    of JSON:
        try:
            json_storage.saveECProgramsForSolvedTask(compressionMessagePath, storageInfo.getECProgramsStoragePath())
        except JsonParsingError:
            echo("Saving ec programs for '" & compressionMessagePath & "' failed: " & repr(getCurrentException()))
            return false
    if deleteAfterwards:
        discard os.tryRemoveFile(compressionMessagePath)
    return true

proc getCompressionMessagePaths*(): seq[string] =
    result = @[]
    var dcPath = makeAbsolute(os.getEnv($dcPath))
    for kind, path in os.walkDir(dcPath/"compressionMessages"):
        if kind == pcFile:
            result.add(path)

# init storage
proc initJsonStorage()=
    var prettyPrinting = (getEnv($jsonPrettyPrinting)=="TRUE")

    let relativeStoragePath = os.getEnv($storagePath)
    let storagePath = relativeStoragePath.makeAbsolute()

    let tasksSubdir = TASKSUBDIR
    let ecProgramsSubdir = ECPROGRAMSSUBDIR
    let settingsSubDir = SETTINGSSUBDIR
    storageInfo = StorageInfo(storageKind: JSON, storagePath: storagePath,
                              tasksSubdir: tasksSubdir, prettyPrinting: prettyPrinting,
                              ecProgramsSubdir: ecProgramsSubdir, settingsSubDir: settingsSubDir,
                              fileExtension: FILEEXTENSION)

    if not os.dirExists(storagePath):
        os.createDir(storagePath)
        echo("Storage path created: " & storagePath)
    if not os.dirExists(storagePath/tasksSubdir):
        os.createDir(storagePath/tasksSubdir)
        echo("Storage path created: " & storagePath/tasksSubdir)
    if not os.dirExists(storagePath/ecProgramsSubdir):
        os.createDir(storagePath/ecProgramsSubdir)
        echo("Storage path created: " & storagePath/ecProgramsSubdir)
    if not os.dirExists(storagePath/settingsSubdir):
        os.createDir(storagePath/settingsSubdir)
        echo("Storage path created: " & storagePath/settingsSubdir)

    let defaultSettingsFile = storageInfo.getSettingPath("setting_default")
    if not os.fileExists(defaultSettingsFile):
        writeFile(defaultSettingsFile, experiment.defaultSettings)


proc initStorage*() =
    case parseEnum[StorageType](os.getEnv($storageType))
    of JSON:
        initJSONStorage()
