##    doc.nim
##    defines syntactic grammer and commands for CLI
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz
import strformat

# model domains
type CLIIdentifier* = enum
    experimentOp = "experiment"
    taskOp = "task"
    createOp = "create"
    deleteOp = "delete"
    listOp = "list"
    testOp = "test"
    runOp = "run"
    importSolutionOp = "getSolutions"
    translateSolutionOp = "translateSolution"
    nameValue = "<name>"
    settingFileNameValue = "<settingFileName>"
    pathToDataCSVValue = "<pathToDataCSV>"
    taskNameValue = "<taskName>"
    versionOption = "--version"



let docstring* = fmt("""
SYN-Trainer

Usage:
    syntrainer (-h | --help)
    syntrainer {taskOp} {listOp}
    syntrainer {taskOp} {createOp} {nameValue} {pathToDataCSVValue}
    syntrainer {taskOp} {deleteOp} {nameValue}
    syntrainer {experimentOp} {listOp}
    syntrainer {experimentOp} {runOp} {nameValue}
    syntrainer {experimentOp} {createOp} {nameValue} {settingFileNameValue} {taskNameValue}...
    syntrainer {experimentOp} {deleteOp} {nameValue}
    syntrainer {importSolutionOp}
    syntrainer {translateSolutionOp} {taskNameValue}
    syntrainer {versionOption}

Options:
    -h --help       Show this screen
    {versionOption}       Show version
""")
