##    task.nim
##    defining Task type
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import csv_reader, exceptions, types

type Task* = ref object
    name*: string
    data*: seq[IOData]

proc `==`*(ioV1: IOValue, ioV2: IOValue): bool =
    return ioV1.strVal == ioV2.strVal

proc `==`*(data1: IOData, data2: IOData): bool =
    return data1.input == data2.input and
           data1.output == data2.output

proc `!=`*(data1: IOData, data2: IOData): bool =
    return not (data1 == data2)

proc `==`*(task1: Task, task2: Task): bool =
    if task1.isNil and task2.isNil:
        return true
    if cast[pointer](task1) == cast[pointer](task2):
        return true
    if task1.isNil or task2.isNil:
        return false
    if task1.name != task2.name:
        return false
    if task1.data.len != task2.data.len:
        return false
    for index in 0..<task1.data.len:
        if task1.data[index] != task2.data[index]:
            return false
    return true

proc `!=`*(task1: Task, task2: Task): bool =
    return not (task1 == task2)

##Call "task$<int>" to give the second argument
proc `$`*(task: Task, elementsToShow: int = 3): string =
    if task == nil:
        return "Called $task but got nil as argument."
    result = "Task: " & task.name
    let dataCount = task.data.len()
    let limit =  min(dataCount, elementsToShow)
    for i in 0..<limit:
        result &= "\n\tinput: " & $task.data[i].input.strVal
        result &= "\n\toutput: " & $task.data[i].output.strVal & "\n"
    if limit < dataCount:
        result &= "\t... (" & `$`(dataCount-limit) & " not displayed)"


proc newTask*(name: string, pathToDataCSV: string): Task =
    var taskData: seq[IOData]
    try:
        taskData = csv_reader.readIOData(pathToDataCSV)
    except IOError:
        raise newException(InvalidUserInputError, getCurrentExceptionMsg())

    result = Task(name: name, data: taskData)
