##    doc_utility.nim
##    converters (from and to ValueTypes) and templates
##    which are used by interpreter, domains, and translation
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz
import types, sugar, os
from exceptions import ECProgramError

proc toInt*(x: ValueType): int;
proc toStr*(x: ValueType): string;
proc toSeq*(x: ValueType): seq[ValueType];
proc toChar*(x: ValueType): char;

proc toValueType*(strValue: string): ValueType;
proc toValueType*(intValue: int): ValueType;
proc toValueType*(callableValue: (proc (x: ValueType): ValueType)): ValueType;
proc toValueType*(seqVal: seq[ValueType]): ValueType;
proc toValueType*(charVal: char): ValueType;
proc toValueType*(ioValue: IOValue): ValueType;
proc `==`*(a: ValueType, b: ValueType): bool;

template ecProgramAssert*(cond: untyped, msg = "") =
    if not cond:
        raise newException(ECProgramError, msg)

template newCallable*(inputIdent: untyped, inType: ValueKind, outType: ValueKind, body: untyped): Callable =
    # returns a Callable which supports ecProgramAssert for the input and output kind
    (inputIdent: ValueType) => (
        ecProgramAssert(inputIdent.kind == inType, "type check failed!");
        var res = toValueType(body);
        ecProgramAssert(res.kind == outType, "type check failed");
        return res
    )

template defineCallablePrimitive*(primitiveName: untyped,
        inputIdent: untyped, inType: ValueKind,
        outType: ValueKind, body: untyped) =
    ## template to define a primitive; inserts ecProgramAssert for the input and output kind
    proc primitiveName(inputIdent: ValueType): ValueType =
        ecProgramAssert(inputIdent.kind == inType, "type check failed!")
        var res = toValueType(body)
        ecProgramAssert(res.kind == outType, "type check failed!")
        return res

template defineConstPrimitive*(primitiveName: untyped,
        outType: ValueKind, body: untyped) =
    proc primitiveName(inputIdent: ValueType): ValueType =
        #TODO: echo message for when inputIdent != ecNone
        #TODO: muss im header was stehen? funktioniert bei mir nur so, wundert mich aber
        return toValueType(body)


proc toInt*(x: ValueType): int =
    ecProgramAssert x.kind == ecInt, $x & "is not of kind ecInt"
    return x.intVal

proc toStr*(x: ValueType): string =
    ecProgramAssert x.kind == ecString, $x & "is not of kind ecString"
    return x.stringVal

proc toSeq*(x: ValueType): seq[ValueType] =
    ecProgramAssert x.kind == ecSeq, $x & "is not of kind ecSeq"
    return x.seqVal

proc toChar*(x: ValueType): char =
    ecProgramAssert x.kind == ecChar, $x & "is not of kind ecChar"
    return x.charVal

proc toValueType*(strValue: string): ValueType =
  result = ValueType(kind: ecString, stringVal: strValue)

proc toValueType*(intValue: int): ValueType =
  result = ValueType(kind: ecInt, intVal: intValue)

proc toValueType*(callableValue: (proc (x: ValueType): ValueType)): ValueType =
  result = ValueType(kind: ecCallable, callableVal: callableValue)

proc toValueType*(seqVal: seq[ValueType]): ValueType =
  result = ValueType(kind: ecSeq, seqVal: seqVal)

proc toValueType*(charVal: char): ValueType =
  result = ValueType(kind: ecChar, charVal: charVal)

proc toValueType*(ioValue: IOValue): ValueType =
  case ioValue.kind:
  of ioString:
      result = ValueType(kind: ecString, stringVal: ioValue.strVal)

proc `==`*(a: ValueType, b: ValueType): bool =
    if a.kind != b.kind:
        return false
    case a.kind:
        of ecInt:
            result = a.intVal == b.intVal
        of ecString:
            result = a.stringVal == b.stringVal
        of ecCallable:
            result = a.callableVal == b.callableVal
        of ecSeq:
            result = a.seqVal == b.seqVal
        of ecChar:
            result = a.charVal == b.charVal
        of ecNone:
            result = true
