##    domains.nim
##    loading domains
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import domains/[text, compile_text] # add further domains

# primitives used by the interpreter and translation
let primitives* = textPrimitives # add further primitives with `concat`

# primitives for compiling synthesized nim code
export compile_text
