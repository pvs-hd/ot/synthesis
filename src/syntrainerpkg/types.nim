##    types.nim
##    Defines types which are used in many modules. Experiment and Task are stored sepratly
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz


import sugar

type
  # used for ec program evaluation
  ValueKind* = enum
    ecInt,
    ecString,
    ecCallable,
    ecSeq,
    ecChar,
    ecNone

  ValueType* = object
    case kind*: ValueKind
    of ecInt: intVal*: int
    of ecString: stringVal*: string
    of ecCallable: callableVal*: (proc (x: ValueType): ValueType)
    of ecSeq: seqVal*: seq[ValueType]
    of ecChar: charVal*: char
    of ecNone: discard

  Environment* = seq[ValueType]

  Callable* = ((ValueType) -> ValueType)

  # used for ec program parsing and translation
  ExpressionNode* = ref object
    ## ``ExpressionNode`` is a node in a tree to resemble lisp expressions
    ## parents will have an empty string as identifier but children
    ## leafs will have a nonempty identifier string but an empty list for children
    ident*: string
    children*: seq[ExpressionNode]

  Expression* = string
  FormattedCode* = string
  FormattedCodeLines* = seq[FormattedCode]

  ProgramFragment* = ref object of RootObj
  ## different type of ``ProgramFragments`` exist in every lisp string generated
  ## by DC. To Translate from ``ExpressionNode`` Tree to a Nim program, we first
  ## need to distinguish the different program fragments like so:
  Abstraction* = ref object of ProgramFragment
      identifier*: Expression
      body*: ProgramFragment
  Application* = ref object of ProgramFragment
      program*: ProgramFragment
      input*: ProgramFragment
  Primitive* = ref object of ProgramFragment
      name*: Expression
      #signature*: seq[ValueKind]
      isConstant*: bool
      callable*: (proc (x: ValueType): ValueType)
  Parameter* = ref object of ProgramFragment
      index*: Expression

  # primarily used for task.nim

  IOValueKind* = enum
      ioString
  IOValue* = object
      case kind* : IOValueKind
      of ioString: strVal* : string

  IOData* = tuple
      input: IOValue
      output: IOValue

  ## types used for API and API specific system functions in controller.nim
  ## SYNResultIdent is used to identify an ecProgram of multiple solutions for one task in cache
  SYNResultIdent* = tuple
      taskName: string
      ecProgramCachePosition: int
  SYNTranslationResult* = object
      ident*: SYNResultIdent
      translationResult*: FormattedCodeLines
  SYNProgramResult* = object
      ident*: SYNResultIdent
      programResult*: ProgramFragment
