##    csv_reader.nim
##    simple csv_reader for nim
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz
import parsecsv
from streams import newFileStream
import types, domain_utility

let Delimiter = ';'

##Reads the first two columns of the CSV file into a IOData sequence
proc readIOData*(pathToDataCSV: string) : seq[IOData] =
    var stream = newFileStream(pathToDataCSV, fmRead)
    if stream == nil:
        raise newException(IOError, "File not found or unable to open: " & pathToDataCSV)
    var parser: CsvParser
    parser.open(stream, pathToDataCSV, Delimiter)

    while readRow(parser):
        var row = parser.row
        var input = IOValue(kind: ioString, strVal: row[0])
        var output = IOValue(kind: ioString, strVal: row[1])
        result.add((input, output))
    parser.close()

proc readValueTypes*(filePath: string): (seq[ValueType], seq[ValueType]) =
    #TODO: maybe redundant
    let fileData = readIOData(filePath)
    var inputData: seq[ValueType]
    var outputData: seq[ValueType]
    for dataPoint in fileData:
        inputData.add(toValueType(dataPoint[0]))
        outputData.add(toValueType(dataPoint[1]))
    return (inputData, outputData)
