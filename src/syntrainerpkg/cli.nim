##    cli.nim
##    cli tool for manual syntrainer usage
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz
import docopt, doc, times, sequtils, options
import experiment, task, controller, exceptions


proc renderList(list: seq[string]) =
    for element in list:
        echo("\t"&element)
    if list.len() == 0:
        echo("Nothing here ..")

proc renderTask(taskRes: Option[Task]) =
    if taskRes.isSome:
        echo(taskRes.get()$5)
    else:
        echo "No task was created."

proc renderExperiment(experimentRes: Option[Experiment]) =
    if experimentRes.isSome:
        echo(experimentRes.get()$5)
    else:
        echo "No experiment was created."

template runCommand(body : typed) =
    let time = cpuTime()
    try:
        body
    except InvalidUserInputError:
        echo("Invalid input: " & getCurrentExceptionMsg())
    except InvalidSettingsError:
        echo("Invalid experiment settings: " & getCurrentExceptionMsg())
    except StorageError:
        echo("Storage operation failed: " & getCurrentExceptionMsg())
    except:
        echo("An error occurred: \n" & repr(getCurrentException()))
        echo("Please create an issue and contact the developers.")
    finally:
        echo("Finished job, elapsed time: ", cpuTime() - time);


proc run*(params: seq[TaintedString], version: string) =
    # parse commands syntactically
    let args = docopt(docstring, version = "SYN-Trainer " & version, argv = params)

    if args[$taskOp]:
        if args[$createOp]:
            runCommand:
                renderTask(controller.createAndSaveTask($args[$nameValue], $args[$pathToDataCSVValue]))
        elif args[$listOp]:
            runCommand:
                renderList(controller.listTasks())
        elif args[$deleteOp]:
            runCommand:
                controller.deleteTask($args[$nameValue])
    if args[$experimentOp]:
        if args[$listOp]:
            runCommand:
                renderList(controller.listExperiments())
        elif args[$runOp]:
            runCommand:
                runExperiment($args[$nameValue])
        elif args[$createOp]:
            runCommand:
                let taskNames = toSeq(args[$taskNameValue])
                let newExperimentRes = controller.createAndSaveExperiment($args[$nameValue], $args[$settingFileNameValue], taskNames)
                renderExperiment(newExperimentRes)
        elif args[$deleteOp]:
            runCommand:
                controller.deleteExperiment($args[$nameValue])

    if args[$importSolutionOp]:
        runCommand:
            controller.importECProgramsOfSolvedTasks()
    if args[$translateSolutionOp]:
        runCommand:
            var translation = controller.getTranslatedECProgramOfSolvedTask($args[$taskNameValue])
            if translation.isSome:
                renderList(translation.get())
            else: echo "No translation returned by system function."
