##    exceptions.nim
##    defining exceptions
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

type
    StorageError* = object of IOError
    InvalidUserInputError* = object of IOError
    InvalidSettingsError* = object of IOError
    #Only raised in json_storage and caught in storage
    NotFoundInStorageError* = object of IOError
    # only raised if interpreting of an ec program failed (interpreter)
    ECProgramError* = object of AssertionDefect
