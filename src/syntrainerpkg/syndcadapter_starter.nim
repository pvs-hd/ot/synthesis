##    syndcadapter_starter.nim
##    This file starts the syndcadapter that is located in the bin directory of the dreamcoder as a new process
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import system, os, strUtils, osproc
import environment_variables
from helpers import makeAbsolute

const DC_BIN_DIR = "bin"
const SYNDCADAPTER_FILENAME = "syndcadapter.py"

var synDCAdapterProcesses: seq[Process] = @[]

proc startSynDcAdapter*(experimentName: string, commandString: string);
proc createProcess(expName : string, commandString: string): Process;

#Helpers
proc seperateCommandAndArgs(command: string) : (string, seq[string]);
#End: Helpers


proc startSynDcAdapter*(experimentName: string, commandString: string) =
    ## Starts a python process that runs the syndcadapter
    ## `experimentName` is the experiment to be run.
    ## `commandString` is the command for the terminal (eg. python3) or multiple commands seperated by `;` (eg. container.img;python3)
    ##
    ## Note:
    ## -Reads DCPATH from .env file as the working directory for the process
    ## -Reads STORAGEPATH from .env file to give it as an argument to syndcadapter.py
    ## -Reads WRITE_DCADAPTER_ON_CONSOLE from .env file and adjusts the process options accordingly

    synDCAdapterProcesses.add(createProcess(experimentName, commandString))

proc createProcess(expName : string, commandString: string): Process =

    var command: string
    var args: seq[string]
    (command, args) = seperateCommandAndArgs(commandString)

    args.add(DC_BIN_DIR/SYNDCADAPTER_FILENAME)
    args.add(expName)
    args.add(makeAbsolute(os.getEnv($storagePath)))

    var options: set[ProcessOption] = {poStdErrToStdOut, poEchoCmd, poUsePath}

    let writeOnConsole = (os.getEnv($dcAdapterOutputOnConsole)=="TRUE")
    if writeOnConsole:
        options.incl(poParentStreams)

    let workingDir = makeAbsolute(os.getEnv($dcPath))
    #echo "Running in: " & workingDir
    result = startProcess(command, workingDir, args, nil, options)

proc waitForExitOfAllProcesses*() =
    #TODO: why is waitForExit not working? (it never finishes)
    # That's why we kill the process here but it's not a clean solution.

    #echo "[DEBUG]: Closing " & $synDCAdapterProcesses.len() & " process(es)."
    for process in synDCAdapterProcesses:
        #var exitCode = 0
        if process.peekExitCode() == -1:
            #echo "[DEBUG] (peekExitCode()==-1)"
            #echo "[DEBUG] killing process"
            process.kill()
            #echo "[DEBUG] Waiting for process to end ..."       
            #exitCode = process.waitForExit(-1)
        #echo "Exit code of DC-process: " & $exitCode & "\n"

        process.close()


#Helpers definition:

proc seperateCommandAndArgs(command: string) : (string, seq[string]) =
    var splits = command.split(';')

    var cmd = splits[0]
    var args = splits[1..<splits.len()]

    return (cmd, args)
