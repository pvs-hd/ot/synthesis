##    cashe.nim
##    creates a cashe for instant use of synthesis
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz
import sequtils, sugar, options, tables
import storage, translation, types, task, interpreter, domain_utility

# boundaries for findSolvedTaskByIOExamples
const checkMaxIOData = 5
const solveMinIOData = 2
const findMaxECPrograms = 2

var ecProgramsByTaskName = initTable[string, seq[ProgramFragment]]()
var ecProgramExpressionsByTaskName = initTable[string, seq[Expression]]()
var initialized = false


proc isInitialized*(): bool =
    result = initialized

proc updateForSolvedTask*(taskName: string) =
    ecProgramExpressionsByTaskName[taskName] = storage.getAllECProgramsForSolvedTask(taskName)
    ecProgramsByTaskName[taskName] = map(ecProgramExpressionsByTaskName[taskName],
        translation.parseECProgramStringToTree)

proc update*() =
    if isInitialized():
        var taskNames = storage.getAllSolvedTaskNames()
        for taskName in taskNames:
            updateForSolvedTask(taskName)

proc init*() =
    update()
    initialized = true

proc findSolvedTaskByIOExamples*(ioDatas: seq[IOData]): seq[SYNProgramResult] =
    # TODO Rewview for Coding Rules: split into orthogonal tasks
    result = @[]
    if solveMinIOData <= len(ioDatas):
        for (taskName, programFragments) in ecProgramsByTaskName.pairs:
            var cachePosition = 0
            for programFragment in programFragments:
                if len(result) == findMaxECPrograms:
                    return result
                var abs = Abstraction(programFragment)
                var countMatches = 0
                for i in countup(0, checkMaxIOData-1):
                    var output = interpreter.call(abs, toValueType(ioDatas[i].input))
                    if output.isSome and IOValue(kind: ioString, strVal: output.get().stringVal) == ioDatas[i].output:
                        countMatches += 1
                    if countMatches == solveMinIOData:
                        break
                if countMatches >= solveMinIOData:
                    result.add(SYNProgramResult(ident: (taskName, cachePosition), programResult: programFragment))
                cachePosition += 1


proc findSolvedTaskByTaskName*(taskName: string): seq[SYNProgramResult] =
    # return all ec programs for solved task found in cache
    if not isInitialized():
        return @[]

    if not ecProgramsByTaskName.hasKey(taskName):
        return @[]

    var cachePosition = -1
    return ecProgramsByTaskName[taskName].map((programFragment: ProgramFragment) => (
        cachePosition += 1;
        return SYNProgramResult(ident: (taskName, cachePosition), programResult: programFragment)
    ))

proc getSYNProgramResultBySYNResultIdent*(ident: SYNResultIdent): Option[SYNProgramResult] =
    if not isInitialized():
        return none(SYNProgramResult)

    if ecProgramsByTaskName.hasKey(ident.taskName) and
        ecProgramsByTaskName[ident.taskName].len > ident.ecProgramCachePosition:

        return some(SYNProgramResult(ident: ident, programResult: ecProgramsByTaskName[ident.taskName][ident.ecProgramCachePosition]))

    return none(SYNProgramResult)
