##    helpers.nim
##    contains helper procedures which are not bounded and often used
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import parseutils, os

proc isInt*(str: string): bool =
    var dummy: int
    str.parseInt(dummy) == str.len()

proc isFloat*(str: string): bool =
    var dummy: float
    str.parseFloat(dummy) == str.len()

proc makeAbsolute*(path: string): string=
    ## The absolute path of the given relative path is returned.
    ## The ortientation of the relative path is the directory of the binary. (`os.getAppDir()`)
    ## If the path is already absolute, nothing happens.
    os.absolutePath(path, os.getAppDir())
