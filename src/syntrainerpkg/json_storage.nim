##    json_storage.nim
##    logic for storing tasks and experiments
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import system, json, os, tables, strformat, sequtils, strutils
import task, experiment, helpers, exceptions, types

const fileExtension = ".json"

proc loadTask*(filePath: string) : Task;
proc loadExperiment*(filePath: string, taskStoragePath: string) : Experiment;
proc saveTask*(task: Task, storagePath: string, prettyPrinting = false);
proc saveExperiment*(experiment: Experiment, storagePath: string, prettyPrinting = false);
proc deleteTask*(filePath: string);
proc deleteExperiment*(filePath: string);
proc loadSettings*(experiment: var Experiment, settingsFile: string);
proc saveECProgramsForSolvedTask*(compressionMessagePath: string, ecProgramsStoragePath: string);
proc loadECPrograms*(filePath: string): seq[string];
proc getAllJsonFiles*(path: string): seq[string];


#Helpers
proc createJsonString(experiment: Experiment): string;
proc createJsonString(task: Task): string;
proc readTasks(tasksArray: JsonNode, taskStoragePath: string): seq[Task];
proc readIOData(ioDataArray: JsonNode): seq[IOData];
proc writeJson(path: string, jsonString: string, prettyPrinting: bool);
proc readDcSettingsTable(settingsTable: JsonNode, fileName: string): OrderedTable[string, string];
proc readJsonObject(filePath: string): JsonNode;
proc getNameFromPath(filePath: string): string =
    splitFile(filePath).name
proc createFilePath(storagePath: string, fileName: string): string =
    storagePath/fileName&fileExtension
proc escapeString(s: string): string =
    s.replace('"', '\'')
#End: Helpers


proc deleteTask*(filePath: string) =
    if not os.fileExists(filePath):
        raise newException(NotFoundInStorageError, "Task '" & getNameFromPath(filePath) & "' not found.")
    os.removeFile(filePath)

proc deleteExperiment*(filePath: string)=
    if not os.fileExists(filePath):
        raise newException(NotFoundInStorageError, "Experiment '" & getNameFromPath(filePath) & "' not found.")
    os.removeFile(filePath)

proc loadTask*(filePath: string) : Task =
    let jsonTask = readJsonObject(filePath)

    let taskName = jsonTask["name"].getStr()

    let taskData = readIoData(jsonTask["data"])

    result = Task(name: taskName, data: taskData)

proc loadExperiment*(filePath: string, taskStoragePath: string) : Experiment  =
    ##filePath: path to the experiment
    ##taskStoragePath: path to the directory where all tasks are stored

    let jsonExp = readJsonObject(filePath)

    let expName = jsonExp["name"].getStr()
    let expDomain = jsonExp["domain"].getStr()
    let expOutdir = jsonExp["outdir"].getStr()

    let expTasks = readTasks(jsonExp["tasks"], taskStoragePath)

    var expSettings = readDcSettingsTable(jsonExp["settings"], filePath)

    result = Experiment(name: expName, domain: expDomain,
                        tasks: expTasks, outdir: expOutdir, settings: expSettings)

proc loadSettings*(experiment: var Experiment, settingsFile: string)=
    var jsonSettings = readJsonObject(settingsFile)

    experiment.domain = jsonSettings["domain"].getStr()
    experiment.outdir = jsonSettings["outdir"].getStr()

    experiment.settings = readDcSettingsTable(jsonSettings["settings"], settingsFile)

proc loadECPrograms*(filePath: string): seq[string] =

    var jsonECPrograms = readJsonObject(filePath)["programs"].getElems()

    var ecPrograms : seq[string]
    for jsonNode in jsonEcPrograms:
        ecPrograms.add(jsonNode["program"].getStr())

    return ecPrograms


proc saveTask*(task: Task, storagePath: string, prettyPrinting = false) =

    var stringTask = createJsonString(task)

    writeJson(createFilePath(storagePath, task.name), stringTask, prettyPrinting)

proc saveExperiment*(experiment: Experiment, storagePath: string, prettyPrinting = false) =

    var stringExp = createJsonString(experiment)

    writeJson(createFilePath(storagePath, experiment.name), stringExp, prettyPrinting)

proc saveECProgramsForSolvedTask*(compressionMessagePath: string, ecProgramsStoragePath: string) =

    var jsonCompressionMessage = readJsonObject(compressionMessagePath)

    for frontier in jsonCompressionMessage["frontiers"]:
        writeJson(createFilePath(ecProgramsStoragePath, frontier["task"].getStr()), $frontier, false)

#Helpers definition:
proc createJsonString(experiment: Experiment): string =
    result = fmt("""{"{"}"name": "{escapeString(experiment.name)}", "domain": "{escapeString(experiment.domain)}",
                        "outdir": "{escapeString(experiment.outdir)}", "settings": {"{"} """)
    #write settings
    var firstEntry = true
    for key, value in experiment.settings.pairs:
        if not firstEntry:
            result &= ", "
        if isFloat(value): #leave out quotation marks
            result &= fmt(""""{escapeString(key)}": {value} """)
        else:
            result &= fmt(""""{escapeString(key)}": "{escapeString(value)}" """)
        firstEntry = false
    result &= """}, "tasks": ["""

    #write tasks
    firstEntry = true
    for task in experiment.tasks:
        if not firstEntry:
            result &= ", "
        result &= fmt(""""{escapeString(task.name)}"""")
        firstEntry = false
    result &= "]}"

proc createJsonString(task: Task): string =
    result = fmt("""{"{"}"name": "{escapeString(task.name)}", "data": [""")
    var firstEntry = true
    for ioData in task.data:
        if not firstEntry:
            result &= ", "

        result &= fmt("""{"{"} "i": "{escapeString(ioData.input.strVal)}", "o": "{escapeString(ioData.output.strVal)}" """)
        result &= "}"
        firstEntry = false
    result &= "]}"

proc writeJson(path: string, jsonString: string, prettyPrinting: bool) =
    if prettyPrinting:
        var jsonObj: JsonNode
        try:
            jsonObj = parseJson(jsonString) #verifies valid json
        except JsonParsingError:
            raise newException(JsonParsingError,"Json parsing failed. Attempted to write to: " & path & "\nMore information: " & getCurrentExceptionMsg())
        writeFile(path, pretty(jsonObj, indent=4))
    else:
        writeFile(path, jsonString)

proc readJsonObject(filePath: string): JsonNode =
    var jsonString = ""
    try:
        jsonString = readFile(filePath)
    except IOError:
        raise newException(NotFoundInStorageError, "Json file not found or unable to read: " & filePath)

    parseJson(jsonString)


proc getAllFiles*(path: string): seq[string] =
    let filesInPath = toSeq(walkDir(path, relative=true))
    for (_, file) in filesInPath:
        result.add(getNameFromPath(file))

proc getAllJsonFiles*(path: string): seq[string] =
    let filesInPath = toSeq(walkDir(path, relative=true))
    for (_, file) in filesInPath:
        if file.endsWith(fileExtension):
            result.add(getNameFromPath(file))

proc readTasks(tasksArray: JsonNode, taskStoragePath: string): seq[Task] =
    var jsonTasksArray: seq[JsonNode] = tasksArray.getElems()

    for taskNameJsonNode in jsonTasksArray:
        var taskName = taskNameJsonNode.getStr()
        var taskPath = createFilePath(taskStoragePath, taskName)

        var task = loadTask(taskPath)
        result.add(task)

proc readIOData(ioDataArray: JsonNode): seq[IOData] =
    var jsonDataArray: seq[JsonNode] = ioDataArray.getElems()

    for ioExampleJsonNode in jsonDataArray:
        let inputEx = IOValue(kind: ioString, strVal: ioExampleJsonNode["i"].getStr())
        let outputEx = IOValue(kind: ioString, strVal: ioExampleJsonNode["o"].getStr())
        result.add((inputEx, outputEx))


proc readDcSettingsTable(settingsTable: JsonNode, fileName: string): OrderedTable[string, string] =
    ##fileName only for pretty error messages

    var jsonSettingsTable: OrderedTable[string, JsonNode] = settingsTable.getFields()
    for (key, value) in jsonSettingsTable.pairs:
        if value.kind == JString:
            result[key] = value.getStr()
        elif value.kind == JInt:
            result[key]= $value.getInt()
        elif value.kind == JFloat:
            result[key]= $value.getFloat()
        else:
            raise newException(JsonParsingError, "No valid value type for '" & key & "' in settings: " & fileName)
