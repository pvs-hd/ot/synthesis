##    interpreter.nim
##    interpreter for EC programs. This module interprets program tree
##    abstractions parsed by translation.nim
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import types, sugar, strutils, sequtils, options
from exceptions import ECProgramError

method interpret(programFrag: ProgramFragment, ctx: Environment): Callable {. base, locks: "unknown" .} = discard
method eval(programFrag: ProgramFragment, ctx: Environment): ValueType {. base, locks: "unknown" .} = discard

method interpret(prim: Primitive, ctx: Environment): Callable {. locks: "unknown" .};
method interpret(abst: Abstraction, ctx: Environment): Callable {. locks: "unknown" .};
method interpret(param: Parameter, ctx: Environment): Callable {. locks: "unknown" .};
method interpret(app: Application, ctx: Environment): Callable {. locks: "unknown" .};

method eval(abst: Abstraction, ctx: Environment): ValueType {. locks: "unknown" .};
method eval(prim: Primitive, ctx: Environment): ValueType {. locks: "unknown" .};
method eval(param: Parameter, ctx: Environment): ValueType {. locks: "unknown" .};
method eval(app: Application, ctx: Environment): ValueType {. locks: "unknown" .};

proc call*(abst: Abstraction, input: ValueType): Option[ValueType];

method interpret(prim: Primitive, ctx: Environment): Callable =
    return prim.callable

method interpret(abst: Abstraction, ctx: Environment): Callable =
    return (x: ValueType) => eval(abst.body, concat(@[x], ctx))

method interpret(param: Parameter, ctx: Environment): Callable =
    raise newException(ECProgramError,
        "A parameter cannot be interpreted. A parameter can only be evaluated. Looks like the ec program is wrong.")

method interpret(app: Application, ctx: Environment): Callable =
    raise newException(ECProgramError,
        "A primitive cannot be interpreted. A primitive can only be evaluated. Looks like the ec program is wrong.")

method eval(abst: Abstraction, ctx: Environment): ValueType =
    return ValueType(kind: ecCallable, callableVal: interpret(abst, ctx))

method eval(prim: Primitive, ctx: Environment): ValueType =
    if prim.isConstant:
        return prim.callable(ValueType(kind: ecNone)) # constant primitive like '0' or '.'
    else:
        return ValueType(kind: ecCallable, callableVal: prim.callable)

method eval(param: Parameter, ctx: Environment): ValueType =
    return ctx[parseInt(param.index)]

method eval(app: Application, ctx: Environment): ValueType =
    var evaluatedInput: ValueType = eval(app.input, ctx)
    if app.program of Abstraction or app.program of Primitive:
        var interpretedProgram: Callable = interpret(app.program, ctx)
        var test = interpretedProgram(evaluatedInput)
        return test
    elif app.program of Application:
        var appResult: ValueType = eval(app.program, ctx)
        assert appResult.kind == ecCallable
        return appResult.callableVal(evaluatedInput)

proc call*(abst: Abstraction, input: ValueType): Option[ValueType] =
    var ctx: Environment= @[]
    try:
        var val = eval(abst, ctx).callableVal(input)
        result = some(val)
    except ECProgramError, IndexDefect:
        result = none(ValueType)
