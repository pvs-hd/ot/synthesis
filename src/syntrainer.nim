##    SYN-Trainer
##    This is the main entry point.
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import syntrainerpkg/[api, domains, domain_utility]
import syntrainerpkg/domains
import syntrainerpkg/domain_utility

export api, domain_utility
export domains except primitives

when isMainModule:
    # start main routine with CLI
    api.initialize(true)
    api.close()
