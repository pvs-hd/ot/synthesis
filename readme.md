![](docs/logo.jpeg)

# Readme

## Usage
Make sure you have DreamCoder (DC) installed. If not, run `dreamcoder_files/install.py` to install it on a server.

### API
For integration of the syntrainer into your project you can use the API for an easy access as a module. Just `import syntrainer` for easy access to the whole api.  

First: Call `initialize()` to setup the syntrainer.  
After that you can use all procedures in the api.  

`createAndRunTasksInNewExperiment`: create and run tasks in a new Experiment. Experiments are bundles of tasks with config data to send to DC. Tasks consist of Trainingdata (check task.nim and experiment.nim for details)  
`update()` use this frequently to check the progress DC is making  
`getSolvedTask()` returns the last solved Task  
`close()` waits for syndcadapter to finish and stop the processes  

(For a more detailed description, visit the [API section in the documentation](docs/documentation.md#api-apinim))

### CLI
If you call `initialize(true)`, a terminal will start where you can use `syntrainer --help` for help.  
From here you can create and delete Tasks or Experiments, view them and run them. You can also view the translation.

## Important Links
[Documentation](docs/documentation.md)  
[Follow-Up Developer Documentation](docs/follow-up_developer_documentation.md)


