#    create_testdata.py
#    creates csv files for tb1_cases to run DreamCoder with
#    This file is part of the synthesis project.
#    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import csv, os.path, sys

from dreamcoder_files.bin import text

def txt_to_array(txt_file, quote_char='"'):
    file = open(txt_file)
    lines_in_file = file.readlines()
    lines_in_file = [line.rstrip('\n\r') for line in lines_in_file]
    return lines_in_file


def compute(data, function, max_length=1):
    input = []
    output = []
    for line in data[0:min(len(data), max_length)]:
        input.append(line)
        output.append(function(line))
    return input, output


def dump_csv(output_file, input, output):
    with open(output_file, 'w+', newline='') as file:
        test_writer = csv.writer(file, delimiter=';')
        for example in range(len(input)):
            test_writer.writerow([input[example], output[example]])


def write_csv_files(input_file, function, output_path, output_file, max_length):
    if not os.path.exists(output_path):
        os.mkdir(output_path)

    data = txt_to_array(input_file)
    input, output = compute(data, function, max_length)
    dump_csv(output_path + output_file, input, output)


def write_all_tb1_files(length, output_path='in_out_data'):
    path_to_rawdata = "tb1_cases/raw_datasets"
    path_to_names = os.path.join(path_to_rawdata, "names.txt")

    surname = lambda x: text._nthString(0)(text._split(',')(x))
    write_csv_files(path_to_names, surname, output_path, '/tb1_surname_' + str(length) + '.csv', length)

    initials = lambda x: text._concatenate(text._nthChar(0)(text._nthString(1)(text._split(',')(x))))(
        text._nthChar(0)(text._nthString(0)(text._split(',')(x))))
    write_csv_files(path_to_names, initials, output_path, '/tb1_initials_' + str(length) + '.csv', length)

    upper = lambda x: text._upper(x)
    write_csv_files(path_to_names, upper, output_path, '/tb1_upper_' + str(length) + '.csv', length)

    firstCharAfterColon = lambda x: text._nthChar(0)(text._nthString(1)(text._split(',')(x)))
    write_csv_files(path_to_names, firstCharAfterColon, output_path,
                    '/tb1_1stcharAfterColon_' + str(length) + '.csv', length)

    firstAndThirdChar = lambda x: text._concatenate(text._nthChar(0)(x))(text._nthChar(2)(x))
    write_csv_files(path_to_names, firstAndThirdChar, output_path,
                    '/tb1_1st+3rd_char_' + str(length) + '.csv', length)

    firstAndThirdCharAfterColon = lambda x: text._concatenate(
        text._nthChar(0)(text._nthString(0)(text._split(',')(x))))(
        text._nthChar(2)(text._nthString(1)(text._split(',')(x))))
    write_csv_files(path_to_names, firstAndThirdCharAfterColon, output_path,
                    '/tb1_1st+3rdcharAfterColon_' + str(length) + '.csv', length)

    firstCharAndTwoCharsAfterColon = lambda x: text._concatenate(text._nthChar(0)(x))(
        text._concatenate(text._nthChar(0)(text._nthString(1)(text._split(',')(x))))(
            text._nthChar(1)(text._nthString(1)(text._split(',')(x)))))
    write_csv_files(path_to_names, firstCharAndTwoCharsAfterColon, output_path,
                    '/tb1_1stchar+2charsAfterColon_' + str(length) + '.csv', length)

    twoCharsAfterColon = lambda x: text._concatenate(text._nthChar(0)(text._nthString(1)(text._split(',')(x))))(
        text._nthChar(1)(text._nthString(1)(text._split(',')(x))))
    write_csv_files(path_to_names, twoCharsAfterColon, output_path,
                    '/tb1_2charsAfterColon_' + str(length) + '.csv', length)

    twoFirstCharsAndTwoCarsAfterColon = lambda x: text._concatenate(
        text._concatenate(text._nthChar(0)(text._nthString(0)(text._split(',')(x))))(
            text._nthChar(1)(text._nthString(0)(text._split(',')(x)))))(
        text._concatenate(text._nthChar(0)(text._nthString(1)(text._split(',')(x))))(
            text._nthChar(1)(text._nthString(1)(text._split(',')(x)))))
    write_csv_files(path_to_names, twoFirstCharsAndTwoCarsAfterColon, output_path,
                    '/tb1_2firstChars+2charsAfterColon_' + str(length) + '.csv', length)

    secondAndFourthChar = lambda x: text._concatenate(text._nthChar(1)(x))(text._nthChar(3)(x))
    write_csv_files(path_to_names, secondAndFourthChar, output_path,
                    '/tb1_2nd+4th_char_' + str(length) + '.csv', length)

    thirdChar = lambda x: text._nthChar(2)(x)
    write_csv_files(path_to_names, thirdChar, output_path, '/tb1_3rd_char_' + str(length) + '.csv', length)

    concat = lambda x: text._concatenate(text._nthString(0)(text._split(',')(x)))(
        text._nthString(2)(text._split(',')(x)))

    path_to_concat_names = os.path.join(path_to_rawdata, "concat_names.txt")
    write_csv_files(path_to_concat_names, concat, output_path, '/tb1_concat_' + str(length) + '.csv', length)

    path_to_emails = os.path.join(path_to_rawdata, "email.txt")
    email = lambda x: text._nthString(0)(text._split('@')(text._nthString(1)(text._split('.')(x))))
    write_csv_files(path_to_emails, email, output_path, '/tb1_email_' + str(length) + '.csv', length)

    firstChar = lambda x: text._nthChar(0)(x)
    write_csv_files(path_to_names, firstChar, output_path, '/tb1_firstChar_' + str(length) + '.csv', length)

    initialsInOrder = lambda x: text._concatenate(text._nthChar(0)(text._nthString(0)(text._split(',')(x))))(
        text._nthChar(0)(text._nthString(1)(text._split(',')(x))))
    write_csv_files(path_to_names, initialsInOrder, output_path,
                    '/tb1_initialsInOrder_' + str(length) + '.csv', length)

    lastname = lambda x: text._nthString(1)(text._split(',')(x))
    write_csv_files(path_to_names, lastname, output_path, '/tb1_lastname_' + str(length) + '.csv', length)

    lower = lambda x: text._lower(x)
    write_csv_files(path_to_names, lower, output_path, '/tb1_lower_' + str(length) + '.csv', length)

    substring = lambda x: text._prefix(4)(x)
    write_csv_files(path_to_names, substring, output_path, '/tb1_substring_' + str(length) + '.csv', length)

    substring2 = lambda x: text._sliceString(2)(4)(x)
    write_csv_files(path_to_names, substring2, output_path, '/tb1_substring2_' + str(length) + '.csv',
                    length)

    path_to_names_spaces = os.path.join(path_to_rawdata, "names_spaces.txt")
    surnameSpace = lambda x: text._nthString(0)(text._split(' ')(x))
    write_csv_files(path_to_names_spaces, surnameSpace, output_path,
                    '/tb1_surname_space_' + str(length) + '.csv', length)

    swap = lambda x: text._concatenate(text._concatenate(text._nthString(1)(text._split(',')(x)))(','))(
        text._nthString(0)(text._split(',')(x)))
    write_csv_files(path_to_names, swap, output_path, '/tb1_swap_' + str(length) + '.csv', length)


if __name__ == "__main__":
    out_dir = sys.argv[1]
    write_all_tb1_files(length=10, output_path = out_dir)
    write_all_tb1_files(length=20, output_path = out_dir)
