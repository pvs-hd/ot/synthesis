##    textdomain.py
##    Here are all primitives defined that the DC can use within the dextdomain.
##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

from dreamcoder.program import Primitive
from dreamcoder.type import arrow, tint, tcharacter, tstr, tlist, tboolean
from dreamcoder.domains.text.makeTextTasks import delimiters
from dreamcoder.domains.list.listPrimitives import bootstrapTarget

import text

primitives = [
    Primitive("lower", arrow(tstr, tstr), text._lower),
    Primitive("upper", arrow(tstr, tstr), text._upper),
    Primitive("concatenate", arrow(tstr, tstr, tstr), text._concatenate),
    Primitive("nthString", arrow(tint, tlist(tstr), tstr), text._nthString),
    Primitive("split", arrow(tcharacter, tstr, tlist(tstr)), text._split),
    Primitive("sliceString", arrow(tint, tint, tstr, tstr), text._sliceString),
    Primitive("incr", arrow(tint, tint), text._incr)
    #Primitive("prefix", arrow(tint, tstr, tstr), _prefix),
    #Primitive("firstPrefix", arrow(tstr, tstr), _firstPrefix)
]

specialCharacters = {' ': 'SPACE',
                     ')': 'RPAREN',
                     '(': 'LPAREN'}

#primitives += [Primitive("STRING", tstr, None)]
primitives += [Primitive("'%s'" % d, tcharacter, d) for d in delimiters if d not in specialCharacters]
#primitives += [Primitive(name, tcharacter, value) for value, name in specialCharacters.items()]
primitives += [Primitive(str(j), tint, j) for j in range(2)]
#primitives += [p for p in bootstrapTarget() if (p.name != "map" and p.name != "unfold" and p.name != "length")]
