def _lower(x): return x.lower()
def _upper(x): return x.upper()
def _concatenate(x): return lambda y: x + y
def _nthString(n): return lambda x: x[n]
def _firstString(x): return x[0]
def _split(delimiter): return lambda s: s.split(delimiter)
def _eq(x): return lambda y: x == y
def _toString(x): return str(x)
def _nthChar(n): return lambda x: x[n]
def _sliceString(i): return lambda j: lambda s: s[i:j]
def _prefix(i): return lambda w: w[0:i]
def _firstPrefix(x): return x[0]
def _incr(x) : return x+1