##    syndcadapter.py
##    SYN-Trainer uses this file to adapt to the DreamCoder
##    This file is part of the synthesis project.
##    Group members: C. Schoen-Schoeffel, F. von Manstein, J. Borowitz

# imports
import sys, json, os, datetime, re
import binutil
from dreamcoder.dreamcoder import commandlineArguments, ecIterator
from dreamcoder.domains.text.main import LearnedFeatureExtractor, text_options
from dreamcoder.grammar import Grammar
from dreamcoder.task import Task
from dreamcoder.type import arrow, tstr
from dreamcoder.utilities import numberOfCPUs, testTrainSplit
# primitives for TB1Cases
#from dreamcoder.domains.text.textPrimitives import primitives as primitives
from textdomain import primitives



# SYN-DC-Adapter
# identifier in experiment json object
EXPERIMENT_SETTINGS_IDENT = "settings" # where are specific dreamcoder settings
EXPERIMENT_OUTDIR_IDENT = "outdir" # where to store DreamCoder results
EXPERIMENT_DOMAIN_IDENT = "domain" # default "text" domain for TB1Cases
EXPERIMENT_TASKS_IDENT = "tasks" # is array with task names

TASK_EXAMPLE_INPUT_IDENT = "i"
TASK_EXAMPLE_OUTPUT_IDENT = "o"
TASK_EXAMPLE_IDENT = "data"

# hardware specific settings
NUMBER_OF_CPUS = numberOfCPUs()


# usage: python syndcadapter.py <experimentFileName> <syn_storagepath>
# example: python syndcadapter.py experiment1 /home/path/to/syn/storage
# syn-dc-adapter will execute the experiment
def main():

    # get experiment
    experimentFileName = sys.argv[1]
    syn_storagepath = sys.argv[2]

    pathToExperiment = os.path.join(syn_storagepath, experimentFileName+".json")
    experiment = getJsonObject(pathToExperiment)

    settings = experiment[EXPERIMENT_SETTINGS_IDENT]
    settings["CPUs"] = NUMBER_OF_CPUS
    args = settings

    outDir = experiment[EXPERIMENT_OUTDIR_IDENT]
    initOutDir(outDir)
    outPrefix = getOutPrefix(outDir)
    args.update({"outputPrefix": outPrefix})

    training_data = getTasks(experiment[EXPERIMENT_TASKS_IDENT], syn_storagepath)
    #print(training_data)
    tasks = [get_tstr_task(item) for item in training_data]

    #for t in tasks:
    #    t.mustTrain = False
    #test, train = testTrainSplit(tasks, .7)


    #Adding domains TODO: 
    #Add if-statment here to check "domain" and choose primitives accordingly
    grammar = Grammar.uniform(primitives)

    print(primitives)

    resume = getCheckpointPathWithMaxIt(outDir)
    print(resume)
    if args != None:
        args["resume"] = resume

    #args["featureExtractor"] = LearnedFeatureExtractor

    generator = ecIterator(grammar,
                           tasks,
                           #testingTasks=test,
                           # with resume
                           #resume=resume,
                           **args)
    for i, _ in enumerate(generator):
        print('ecIterator count {}'.format(i))

def get_tstr_task(item):
    return Task(
        item["name"],
        arrow(tstr, tstr),
        [((ex[TASK_EXAMPLE_INPUT_IDENT],), ex[TASK_EXAMPLE_OUTPUT_IDENT]) for ex in item[TASK_EXAMPLE_IDENT]],
    )

def getTasks(taskNamesList, storagePath):
    tasks = []
    for taskName in taskNamesList:
        pathToJsonFile = os.path.join(storagePath, "tasks", taskName+".json")
        tasks.append(getJsonObject(pathToJsonFile))
    return tasks

def getCheckpointPathWithMaxIt(pathToCheckpoints):
    resumeIt = 0
    checkpointPath = None
    for fileName in os.listdir(pathToCheckpoints):
        filePath = os.path.join(pathToCheckpoints, fileName)
        if os.path.isfile(filePath) and re.search(".pickle", fileName):
            itMatch = re.search("\d*(?=_MF)", fileName)
            if itMatch:
                it = int(itMatch.group(0))
                if it > resumeIt:
                    resumeIt = it
                    checkpointPath = filePath
    return checkpointPath

def getJsonObject(pathToJsonFile):
    jsonObject = None
    try:
        jsonFile = open(pathToJsonFile, "r")
        try:
            jsonObject = json.load(jsonFile)
        except:
            ...
        finally:
            jsonFile.close()
            return jsonObject
    except (IOError, ValueError, EOFError) as e:
        print(e)

def getOutPrefix(outDir):
    return outDir


def initOutDir(outDir):
    os.makedirs(outDir, exist_ok=True)

if __name__ == "__main__":
    main()
