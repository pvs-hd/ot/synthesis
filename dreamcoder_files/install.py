##    install.py

##    INSTALLATION SCRIPT FOR DREAMCODER (commit: b0c0336)
##    WITH PROGRAM SYNTHESIS DOMAINS
##    USING SINGULARITY TO CREATE A
##    CONTAINER IMAGE

##    This file is part of the synthesis project.
##    Group members: C. Schön-Schöffel, F. von Manstein, J. Borowitz

import os
import shutil

# These files and directories might change with newer versions of this repository (synthesis)
SETUP_FILES_PATH = os.path.abspath("dreamcoder_files")
SINGULARITY_CONFIG = "singularity"
SINGULARITY_CONTAINER = "container.img"
SINGULARITY_EXEC_IMAGE = "singularity exec " + SINGULARITY_CONTAINER
# DC's primitives (to be installed and updated)
OCAML_PROGRAM = "program.ml"
# DC's pro,otoves in python (to be installed)
PYTHON_DOMAINS = ["textdomain.py"]
SYNDCADAPTER = "syndcadapter.py"

# Relvant paths in DC's repository
# These directories might change with newer DC versions
# path to python defined domains
DC_BIN = "bin"
# path to ocaml files
DC_SOLVERS = "solvers"


def init(args):
    if len(args) == 1:
        path_to_dc = os.path.abspath(args[0])
        if os.path.exists(path_to_dc):
            if os.path.isdir(path_to_dc):
                #compile_singularity_container(path_to_dc)
                install_domains_to_dc(path_to_dc)
            else:
                raise NotADirectoryError("Path is not a dictionary!")
        else:
            raise FileNotFoundError("Path " + path_to_dc + "does not exists!")
    else:
        raise ValueError("Expected 1 argument. Got " + str(len(args)) + "!")


def compile_singularity_container(path_to_dc):
    shutil.copy(os.path.join(SETUP_FILES_PATH, SINGULARITY_CONFIG),
                os.path.join(path_to_dc, SINGULARITY_CONFIG))
    os.system("cd " + path_to_dc
              + " & sudo singularity build " + SINGULARITY_CONTAINER + " " + SINGULARITY_CONFIG)



def install_domains_to_dc(path_to_dc):
    shutil.copy(os.path.join(SETUP_FILES_PATH, DC_SOLVERS, OCAML_PROGRAM),
                os.path.join(path_to_dc, DC_SOLVERS, OCAML_PROGRAM))
    shutil.copy(os.path.join(SETUP_FILES_PATH, DC_BIN, SYNDCADAPTER),
                   os.path.join(path_to_dc, DC_BIN, SYNDCADAPTER))
    for domain in PYTHON_DOMAINS:
        shutil.copy(os.path.join(SETUP_FILES_PATH, DC_BIN, domain),
                    os.path.join(path_to_dc, DC_BIN, domain))

    exec_with_singularity(path_to_dc, "make clean")
    exec_with_singularity(path_to_dc, "make all")

def exec_with_singularity(path_to_dc, command):
    os.system("cd " + path_to_dc
              + " & " + command)


if __name__ == "__main__":
    import sys

    init(sys.argv[1:])
